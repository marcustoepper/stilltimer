//
//  StringBuilder.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <Foundation/Foundation.h>

@interface StringBuilder : NSObject

+(NSString *)getLastFeedingString;

+(NSString *)getLastFeedingStartedString;
+(NSString *)getLastFeedingDurationString;

+(NSString *)getAgeString;
+(NSString *)getFeedingsTodayString;

+(NSString *)getStringForSide:(int)side;
+(NSString *)getStringForTime:(double)interval;
+(NSString *)getStringForDuration:(int)duration;
+(NSString *)getStringWithSecondsForDuration:(int)duration;

@end
