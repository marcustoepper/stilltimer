//
//  Settings.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/13/13.
//
//

#import "Settings.h"

@implementation Settings

+(void)wipe {
    [Settings setName:nil];
    [Settings setGender:nil];
    [Settings setBirthday:nil];
    [Settings setPhoto:nil withSource:PHOTO_SOURCE_LIBRARY];
}

+(void)setName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:BABY_NAME_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString *)getName {
    return [[NSUserDefaults standardUserDefaults] objectForKey:BABY_NAME_KEY];
}


+(void)setBirthday:(NSDate *)birthday {
    [[NSUserDefaults standardUserDefaults] setObject:birthday forKey:BABY_BIRTHDAY_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSDate *)getBirthday {
    return [[NSUserDefaults standardUserDefaults] objectForKey:BABY_BIRTHDAY_KEY];
}


+(void)setGender:(NSString *)gender {
    [[NSUserDefaults standardUserDefaults] setObject:gender forKey:BABY_GENDER_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString *)getGender {
    return [[NSUserDefaults standardUserDefaults] objectForKey:BABY_GENDER_KEY];
}


+(void)setPhoto:(UIImage *)photo withSource:(PHOTO_SOURCE)source {
    if (photo != nil) {
        NSData *data = [self prepareImage:photo withSource:source];
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:BABY_PHOTO_FILE_NAME];
        
        NSError *error = nil;
        [data writeToFile:path options:NSDataWritingAtomic error:&error];
        
        if (error != nil) {
            NSLog(@"Error: %@", error);
        }
    }
    else {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:BABY_PHOTO_FILE_NAME];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            
            if (error != nil) {
                NSLog(@"Error: %@", error);
            }
        }
    }
}

+(UIImage *)getPhoto {
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:BABY_PHOTO_FILE_NAME];
    
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

+(NSData *)prepareImage:(UIImage *)image withSource:(PHOTO_SOURCE)source {
    int imageWidth = image.size.width;
    int imageHeight = image.size.height;
    int radius = 137.0*imageWidth/SCREEN_WIDTH;
    if (imageHeight < imageWidth)
        radius = 137.0*imageHeight/SCREEN_HEIGHT;
    
    int centerx = imageWidth/2;
    int centery = imageHeight/2;
    if (source == PHOTO_SOURCE_CAMERA_PORTRAIT) {
        centery += 27*imageHeight/SCREEN_HEIGHT;
    }
    else if (source == PHOTO_SOURCE_CAMERA_PORTRAIT_UPSIDEDOWN) {
        centery -= 27*imageHeight/SCREEN_HEIGHT;
    }
    else if (source == PHOTO_SOURCE_CAMERA_LANDSCAPE_LEFT) {
        centerx -= 27*imageWidth/SCREEN_WIDTH;
    }
    else if (source == PHOTO_SOURCE_CAMERA_LANDSCAPE_RIGHT) {
        centerx += 27*imageWidth/SCREEN_WIDTH;
    }
    
    while (centerx-radius <= 0 || centerx+radius >= imageWidth ||
           centery-radius <= 0 || centery+radius >= imageHeight) {
        radius -= 10;
    }
    
    //helper variables
    int x = centerx-radius;
    int y = centery-radius;
    
    float scale = 130.0/radius;
    
    UIGraphicsBeginImageContext(CGSizeMake(imageWidth*scale, imageHeight*scale));
    [image drawInRect:CGRectMake(0, 0, imageWidth*scale, imageHeight*scale)];
    UIImage *unrotated = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [Settings setPhotoPosition:CGPointMake(-x*scale*0.5, -y*scale*0.5)];
    [Settings setPhotoScale:0.5];
    
    return UIImagePNGRepresentation(unrotated);
}


+(void)setPhotoPosition:(CGPoint)p {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:p.x] forKey:BABY_PHOTO_POSITION_X_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:p.y] forKey:BABY_PHOTO_POSITION_Y_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(CGPoint)getPhotoPosition {
    return CGPointMake([[[NSUserDefaults standardUserDefaults] objectForKey:BABY_PHOTO_POSITION_X_KEY] floatValue], [[[NSUserDefaults standardUserDefaults] objectForKey:BABY_PHOTO_POSITION_Y_KEY] floatValue]);
}


+(void)setPhotoScale:(float)scale {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:scale] forKey:BABY_PHOTO_SCALE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(float)getPhotoScale {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:BABY_PHOTO_SCALE_KEY] floatValue];
}

@end
