//
//  IntroViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "IntroViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)startIntro {
    FirstStepViewController *controller = [[FirstStepViewController alloc] init];
    [self.navigationController pushViewController:controller animated:NO];
}

@end
