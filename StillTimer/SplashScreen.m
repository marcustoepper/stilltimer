//
//  SplashScreen.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/25/13.
//
//

#import "SplashScreen.h"

@interface SplashScreen ()

@end

@implementation SplashScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        int screen_height = [[UIScreen mainScreen] bounds].size.height;
        
        [self.view setFrame:CGRectMake(0, 0, 320, screen_height)];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 320, screen_height)];
        [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        if (screen_height > 480) //4 inch
            [backButton setImage:[UIImage imageNamed:@"Default-568h.png"] forState:UIControlStateNormal];
        else //3.5 inch
            [backButton setImage:[UIImage imageNamed:@"Default.png"] forState:UIControlStateNormal];
        [backButton setAdjustsImageWhenHighlighted:NO];
        [self.view addSubview:backButton];
    }
    return self;
}

-(void)splash {
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.view];
    [self performSelector:@selector(backAction) withObject:nil afterDelay:1.0];
}

-(void)backAction {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self.view removeFromSuperview];
}

@end
