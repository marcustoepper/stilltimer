//
//  IntroViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <UIKit/UIKit.h>
#import "FirstStepViewController.h"

@interface IntroViewController : UIViewController

-(void)startIntro;

@end
