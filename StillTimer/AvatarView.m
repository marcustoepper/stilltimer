//
//  AvatarView.m
//  StillTimer
//
//  Created by Alexander Schilling on 8/26/13.
//
//

#import "AvatarView.h"

@implementation AvatarView

@synthesize target;
@synthesize selector;

@synthesize avatarArea;
@synthesize drawer;

@synthesize refP;

@synthesize tapMoved;

- (id)initWithCenter:(CGPoint)c andEditable:(BOOL)editable andGendered:(BOOL)gendered
{
    self = [super initWithFrame:CGRectMake(c.x - 73.5, c.y - 73.5, 147, 147)];
    if (self) {
        if (editable) {
            [self setUserInteractionEnabled:YES];
        }
        else {
            [self setUserInteractionEnabled:NO];
        }
        
        avatarArea = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        if (gendered) {
            [avatarArea setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BABY_AVATAR_AREA]];
        }
        else {
            [avatarArea setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BABY_AVATAR_AREA_GREY]];
        }
        [self addSubview:avatarArea];
        
        drawer = [[PhotoDrawer alloc] initWithFrame:CGRectMake(8.5, 8.5, 130, 130)];
        [self addSubview:drawer];
        
        [self update];
    }
    return self;
}

-(void)update {
    if ([Settings getPhoto] == nil) {
        [avatarArea setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_NO_PHOTO]];
        [drawer setHidden:YES];
    }
    else {
        [drawer update];
        [drawer setHidden:NO];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    refP = [[touches anyObject] locationInView:self];
    
    tapMoved = NO;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint touch = [[touches anyObject] locationInView:self];
    int xdiff = touch.x-refP.x;
    int ydiff = touch.y-refP.y;
    [drawer applyOffset:CGPointMake(xdiff, ydiff)];
    
    refP = touch;
    
    tapMoved = YES;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [Settings setPhotoPosition:drawer.p];
    [drawer update];
    
    if (!tapMoved) {
        [target performSelector:selector];
    }
}

-(void)setTarget:(id)theTarget selector:(SEL)theSelector {
    target = theTarget;
    selector = theSelector;
}

@end
