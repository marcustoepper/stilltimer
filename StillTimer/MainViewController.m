//
//  MainViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize introController;
@synthesize settingsController;
@synthesize stillController;
@synthesize addEditController;

-(void)loadView {
    self.view = [[MainView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    self.settingsController = [[SettingsViewController alloc] init];
    [self.settingsController setDelegate:self];
    [self.view addSubview:self.settingsController.view];
    
    self.stillController = [[StillViewController alloc] init];
    self.stillController.delegate = self;
    [self.view addSubview:self.stillController.view];
    
    self.addEditController = [[AddEditViewController alloc] init];
    self.addEditController.delegate = self;
    [self.view addSubview:self.addEditController.view];
    [self.addEditController performSelector:@selector(dismiss) withObject:nil afterDelay:0.1];
    
    if ([Settings getName] == nil) {
        [self startIntro];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAddEditFromPullToAdd:) name:ADDEDIT_FROM_PULLTOADD_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAddEditFromListSmall:) name:ADDEDIT_FROM_LISTSMALL_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAddEditFromListLarge:) name:ADDEDIT_FROM_LISTLARGE_NOTIFICATION object:nil];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

-(void)startIntro {
    self.introController = [[IntroViewController alloc] init];
    [self.navigationController pushViewController:self.introController animated:NO];
    [self.introController startIntro];
}

-(void)showAddEditFromPullToAdd:(NSNotification *)notification {
    [self addEditToView:notification];
    [self performSelector:@selector(animateEditIn) withObject:nil afterDelay:0.5];
}

-(void)showAddEditFromListSmall:(NSNotification *)notification {
    [self addEditToView:notification];
    [self animateEditIn];
}

-(void)showAddEditFromListLarge:(NSNotification *)notification {
    [self addEditToView:notification];
    [self.stillController shrinkForEdit];
}

-(void)shrinkForEditDone {
    [self animateEditIn];
}

-(void)animateEditIn {
    if (SCREEN_HEIGHT <= 480) {
        [self.stillController moveUpTopRegion];
    }
    else {
        [self moveUpTopRegionDone];
    }
}

-(void)moveUpTopRegionDone {
    [self.stillController slideDownList];
    [self.addEditController fadeInPicker];
}

-(void)slideDownListDone {
    [self.stillController fadeOutTopRegion];
    [self.addEditController fadeInTopRegion];
}


-(void)animateEditOut {
    [self.stillController fadeInTopRegion];
    [self.addEditController fadeOutTopRegion];
}

-(void)fadeInTopRegionDone {
    [self.stillController slideUpList];
    [self.addEditController fadeOutPicker];
}

-(void)slideUpListDone {
    if (SCREEN_HEIGHT <= 480) {
        [self.stillController moveDownTopRegion];
    }
}


-(void)addEditToView:(NSNotification *)notification {
    int stillTimeId = [[[notification userInfo] objectForKey:@"stillTimeId"] intValue];
    [self.addEditController useStillTimeId:stillTimeId];
    [self.view addSubview:self.addEditController.view];
}


-(void)dismissSettingsView {
    [self.stillController animateIn];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

@end
