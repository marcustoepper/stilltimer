//
//  Settings.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/13/13.
//
//

#import <Foundation/Foundation.h>

typedef enum {
    PHOTO_SOURCE_CAMERA_PORTRAIT,
    PHOTO_SOURCE_CAMERA_PORTRAIT_UPSIDEDOWN,
    PHOTO_SOURCE_CAMERA_LANDSCAPE_LEFT,
    PHOTO_SOURCE_CAMERA_LANDSCAPE_RIGHT,
    PHOTO_SOURCE_LIBRARY
} PHOTO_SOURCE;

@interface Settings : NSObject

+(void)wipe;

+(void)setName:(NSString *)name;
+(NSString *)getName;

+(void)setBirthday:(NSDate *)birthday;
+(NSDate *)getBirthday;

+(void)setGender:(NSString *)gender;
+(NSString *)getGender;

+(void)setPhoto:(UIImage *)photo withSource:(PHOTO_SOURCE)source;
+(UIImage *)getPhoto;

+(void)setPhotoPosition:(CGPoint)p;
+(CGPoint)getPhotoPosition;

+(void)setPhotoScale:(float)scale;
+(float)getPhotoScale;

@end
