//
//  FifthStepViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <UIKit/UIKit.h>
#import "AvatarView.h"

@interface FifthStepViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    AvatarView *avatar;
    
    UIImagePickerController *imagePicker;
}

@end
