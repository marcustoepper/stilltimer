//
//  StillView.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import <UIKit/UIKit.h>
#import "AvatarView.h"
#import "StillTimesListDataSource.h"

@protocol StillViewDelegate <NSObject>

-(BOOL)isTimerRunning;

-(void)startStopButtonPushed:(UIButton *)sender;
-(void)selectSide:(int)side;
-(int)selectedSide;

-(void)showAddEdit:(int)stillTimeId;

-(void)shrinkForEditDone;
-(void)moveUpTopRegionDone;
-(void)fadeInTopRegionDone;
-(void)slideUpListDone;
-(void)slideDownListDone;

@end

@interface StillView : UIView <UITableViewDelegate>

@property (strong, nonatomic) id <StillViewDelegate> delegate;

@property (strong, nonatomic) UIImageView *bg;

@property (strong, nonatomic) UIView *stillView;

@property (strong, nonatomic) UIButton *lButton;
@property (strong, nonatomic) UILabel *lLabel;
@property (strong, nonatomic) UIButton *rButton;
@property (strong, nonatomic) UILabel *rLabel;
@property (strong, nonatomic) UIButton *startStopButton;

@property (strong, nonatomic) UILabel *startedLabel;
@property (strong, nonatomic) UILabel *lastStillLabel;
@property (strong, nonatomic) UILabel *lastStillTitleLabel;
@property (strong, nonatomic) UILabel *durationLabel;

@property (strong, nonatomic) UIView *infoView;

@property (strong, nonatomic) AvatarView *avatar;

@property (strong, nonatomic) UILabel *ageLabel;
@property (strong, nonatomic) UILabel *todayLabel;

@property (strong, nonatomic) UIImageView *plusImage;
@property (strong, nonatomic) UILabel *pullLabel;
@property (strong, nonatomic) UIImageView *noFeedingImage;

@property (strong, nonatomic) StillTimesListDataSource *dataSource;
@property (strong, nonatomic) UITableView *timesTableView;

@property (strong, nonatomic) UIButton *shrinkButton;

-(void)reloadViews;

-(void)updateShrinkButtonAlpha;

-(void)updatePhoto;

-(void)selectOppositeOfLastUsed;

-(void)shrinkForEdit;
-(void)moveUpTopRegion;
-(void)moveDownTopRegion;
-(void)fadeInTopRegion;
-(void)fadeOutTopRegion;
-(void)slideDownList;
-(void)slideUpList;

@end
