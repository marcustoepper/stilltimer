//
//  DVTimePickerView.h
//  StillTimer
//
//  Created by Alexander Schilling on 6/22/13.
//
//

#import "DCPickerView.h"

@interface DCTimePickerView : DCPickerView

//does the localization have AM/PM mode
@property BOOL ampm;


//init method
//point: top left point of the picker
-(id)initAtPoint:(CGPoint)point;


//scrolls to the current time in the local timezone
-(void)scrollToNowAnimated:(BOOL)animated;

//scroll to specified date in the local timezone
-(void)scrollToTime:(NSDate *)time animated:(BOOL)animated;

//get the currently selected time
-(NSDate *)getTime;

@end
