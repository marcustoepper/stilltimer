//
//  StillTimesListCell.m
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import "StillTimesListCell.h"

@implementation StillTimesListCell

@synthesize tableView;

@synthesize scroll;

@synthesize indexPath;

@synthesize durationLabel;
@synthesize sideLabel;
@synthesize startLabel;
@synthesize endLabel;

@synthesize deleteImage;
@synthesize editImage;

@synthesize separator;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
        [background setBackgroundColor:[FontProvider getGenderedFontColor]];
        [self addSubview:background];
        
        self.deleteImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 80)];
        [self.deleteImage setBackgroundColor:[FontProvider getGenderedFontColor]];
        [self.deleteImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_TRASH_ICON]];
        [self.deleteImage setContentMode:UIViewContentModeCenter];
        [self addSubview:self.deleteImage];
        
        self.editImage = [[UIImageView alloc] initWithFrame:CGRectMake(250, 0, 70, 80)];
        [self.editImage setBackgroundColor:[FontProvider getGenderedFontColor]];
        [self.editImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_EDIT_ICON]];
        [self.editImage setContentMode:UIViewContentModeCenter];
        [self addSubview:self.editImage];
        
        self.scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
        [self.scroll setDelegate:self];
        [self.scroll setContentSize:CGSizeMake(SCREEN_WIDTH+1, 80)];
        [self.scroll setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.scroll];
        
        UIView *scrollBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
        [scrollBg setBackgroundColor:[UIColor whiteColor]];
        [self.scroll addSubview:scrollBg];
        
        UIView *lineVerticalLeft = [[UIView alloc] initWithFrame:CGRectMake(90, 0, 1, 80)];
        [lineVerticalLeft setBackgroundColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.scroll addSubview:lineVerticalLeft];
        
        UIView *lineVerticalRight = [[UIView alloc] initWithFrame:CGRectMake(230, 0, 1, 80)];
        [lineVerticalRight setBackgroundColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.scroll addSubview:lineVerticalRight];
        
        separator = [[UIView alloc] initWithFrame:CGRectMake(0, 79, 320, 1)];
        [separator setBackgroundColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self addSubview:separator];
        
        
        self.sideLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, 80, 30)];
        [self.sideLabel setBackgroundColor:[UIColor clearColor]];
        [self.sideLabel setFont:[FontProvider getRegularFontWithSize:22]];
        [self.sideLabel setTextAlignment:NSTextAlignmentCenter];
        [self.sideLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.sideLabel setAdjustsFontSizeToFitWidth:YES];
        [self.scroll addSubview:self.sideLabel];
        
        UILabel *usedBreastLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 45, 90, 20)];
        [usedBreastLabel setText:NSLocalizedString(@"UsedBreastKey", @"")];
        [usedBreastLabel setTextAlignment:NSTextAlignmentCenter];
        [usedBreastLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [usedBreastLabel setFont:[FontProvider getRegularFontWithSize:12]];
        [usedBreastLabel setBackgroundColor:[UIColor clearColor]];
        [self.scroll addSubview:usedBreastLabel];
        
        self.startLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 23, 70, 26)];
        [self.startLabel setBackgroundColor:[UIColor clearColor]];
        [self.startLabel setFont:[FontProvider getRegularFontWithSize:14]];
        [self.startLabel setTextAlignment:NSTextAlignmentCenter];
        [self.startLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.scroll addSubview:self.startLabel];
        
        UILabel *startTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 45, 70, 20)];
        [startTitleLabel setText:NSLocalizedString(@"StartKey", @"")];
        [startTitleLabel setTextAlignment:NSTextAlignmentCenter];
        [startTitleLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [startTitleLabel setFont:[FontProvider getRegularFontWithSize:12]];
        [startTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.scroll addSubview:startTitleLabel];
        
        self.endLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 23, 70, 26)];
        [self.endLabel setBackgroundColor:[UIColor clearColor]];
        [self.endLabel setFont:[FontProvider getRegularFontWithSize:14]];
        [self.endLabel setTextAlignment:NSTextAlignmentCenter];
        [self.endLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.scroll addSubview:self.endLabel];
        
        UILabel *endTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 45, 70, 20)];
        [endTitleLabel setText:NSLocalizedString(@"EndKey", @"")];
        [endTitleLabel setTextAlignment:NSTextAlignmentCenter];
        [endTitleLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [endTitleLabel setFont:[FontProvider getRegularFontWithSize:12]];
        [endTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.scroll addSubview:endTitleLabel];
        
        self.durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(230, 20, 90, 30)];
        [self.durationLabel setBackgroundColor:[UIColor clearColor]];
        [self.durationLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.durationLabel setFont:[FontProvider getRegularFontWithSize:20]];
        [self.durationLabel setTextAlignment:NSTextAlignmentCenter];
        [self.scroll addSubview:self.durationLabel];
        
        UILabel *durationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(230, 45, 90, 20)];
        [durationTitleLabel setText:NSLocalizedString(@"DurationKey", @"")];
        [durationTitleLabel setTextAlignment:NSTextAlignmentCenter];
        [durationTitleLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [durationTitleLabel setFont:[FontProvider getRegularFontWithSize:12]];
        [durationTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self.scroll addSubview:durationTitleLabel];
        
        UIButton *vibrateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [vibrateButton setFrame:CGRectMake(0, 0, 320, 80)];
        [vibrateButton addTarget:self action:@selector(vibrate) forControlEvents:UIControlEventTouchUpInside];
        [self.scroll addSubview:vibrateButton];
    }
    return self;
}

-(void)vibrate {
    [self leftFar];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int x = scrollView.contentOffset.x;
    if (x < 0) {
        [self.deleteImage setAlpha:-(float)x/CELL_PULL_DISTANCE];
    }
    else {
        [self.editImage setAlpha:(float)x/CELL_PULL_DISTANCE];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    int x = scrollView.contentOffset.x;
    
    if (x < -CELL_PULL_DISTANCE) {
        [self deleteTime];
    }
    else if (x > CELL_PULL_DISTANCE) {
        [self editTime];
    }
    [scrollView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, 81) animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [scrollView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, 81) animated:YES];
}


-(void)deleteTime {
    [self deletePrompt];
}

-(void)deletePrompt {
    alert = [[CustomAlertView alloc] initWithMessage:NSLocalizedString(@"DeleteTimePromptKey", @"") buttonTitles:@[NSLocalizedString(@"DeleteTimePromptKeepKey", @""), NSLocalizedString(@"DeleteTimePromptDeleteKey", @"")]];
    alert.delegate = self;
    [alert show];
}

-(void)buttonPushed:(int)buttonId alertViewTag:(int)tag {
    if (buttonId == 0) {
        [self end];
    }
    else {
        [self animateDeletion];
    }
}

-(void)animateDeletion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(deleteEnd)];
    
    for (UIView *view in [self subviews]) {
        [view setFrame:CGRectOffset(view.frame, self.frame.size.width, 0)];
    }
    
    [UIView commitAnimations];
}

-(void)deleteEnd {
    if ([[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:indexPath.section] count] == 1) {
        [[StillData getInstance] deleteStillTimeWithId:[[StillData getInstance] getIdWithIndexPath:self.indexPath]];
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:self.indexPath.section] withRowAnimation:UITableViewRowAnimationTop];
        [self performSelector:@selector(delayedUpdateNotification) withObject:nil afterDelay:0.5];
    }
    else {
        [[StillData getInstance] deleteStillTimeWithId:[[StillData getInstance] getIdWithIndexPath:self.indexPath]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.indexPath] withRowAnimation:UITableViewRowAnimationTop];
        [self performSelector:@selector(delayedUpdateNotification) withObject:nil afterDelay:0.5];
    }
}

-(void)delayedUpdateNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:DATA_UPDATE_NOTIFICATION object:nil];
}

-(void)editTime {
    [self.tableView setUserInteractionEnabled:NO];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(editEnd)];
    
    for (UIView *view in [self subviews]) {
        [view setFrame:CGRectOffset(view.frame, -self.frame.size.width, 0)];
    }
    
    [UIView commitAnimations];
}

-(void)editEnd {
    if (self.tableView.frame.size.height > SCREEN_HEIGHT-304) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ADDEDIT_FROM_LISTLARGE_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:[[StillData getInstance] getIdWithIndexPath:self.indexPath]] forKey:@"stillTimeId"]];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:ADDEDIT_FROM_LISTSMALL_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:[[StillData getInstance] getIdWithIndexPath:self.indexPath]] forKey:@"stillTimeId"]];
    }
}

-(void)reverseAnyAnimation {
    [self.tableView setUserInteractionEnabled:YES];
    
    for (UIView *view in [self subviews]) {
        if (view.frame.origin.x < 0) {
            [view setFrame:CGRectOffset(view.frame, self.frame.size.width, 0)];
        }
        else if (view.frame.origin.x >= 320) {
            [view setFrame:CGRectOffset(view.frame, -self.frame.size.width, 0)];
        }
    }
}


-(void)leftFar {
    [self.deleteImage setAlpha:1.0];
    [self.editImage setAlpha:1.0];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(rightFar)];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    [self.scroll setFrame:CGRectMake(90, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
}

-(void)rightFar {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(leftHalf)];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    [self.scroll setFrame:CGRectMake(-85, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
}

-(void)leftHalf {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(rightHalf)];
    [UIView setAnimationDuration:0.15];
    
    [self.scroll setFrame:CGRectMake(50, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
}

-(void)rightHalf {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(leftShort)];
    [UIView setAnimationDuration:0.15];
    
    [self.scroll setFrame:CGRectMake(-40, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
}

-(void)leftShort {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(rightShort)];
    [UIView setAnimationDuration:0.1];
    
    [self.scroll setFrame:CGRectMake(15, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
}

-(void)rightShort {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(end)];
    [UIView setAnimationDuration:0.1];
    
    [self.scroll setFrame:CGRectMake(-10, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
}

-(void)end {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.05];
    
    [self.scroll setFrame:CGRectMake(0, self.scroll.frame.origin.y, 320, 80)];
    
    [UIView commitAnimations];
    
    [self.deleteImage setAlpha:0.0];
    [self.editImage setAlpha:0.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
}

@end
