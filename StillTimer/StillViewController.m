//
//  StillViewController.m
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import "StillViewController.h"

@interface StillViewController ()

@end

@implementation StillViewController

@synthesize delegate;

@synthesize startedSide;
@synthesize sideChanged;

@synthesize timerRunning;
@synthesize timerStarted;

@synthesize updateInterfaceTimer;

-(void)loadView {
    self.view = [[StillView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [(StillView *)self.view setDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInterface) name:DATA_UPDATE_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self.view selector:@selector(reloadViews) name:SETTINGS_CHANGED_NOTIFICATION object:nil];
    
    [self updateInterface];
    
    self.updateInterfaceTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabels) userInfo:nil repeats:YES];
    
    self.sideChanged = NO;
}

-(void)updateLabels {
    if (self.timerRunning) {
        [[(StillView *)self.view startedLabel] setText:[StringBuilder getStringForTime:self.timerStarted]];
        [[(StillView *)self.view lastStillLabel] setText:NSLocalizedString(@"FeedingInProgressKey", @"")];
        [[(StillView *)self.view durationLabel] setText:[StringBuilder getStringWithSecondsForDuration:[[NSDate date] timeIntervalSince1970]-self.timerStarted]];
    }
    else {
        [[(StillView *)self.view startedLabel] setText:[StringBuilder getLastFeedingStartedString]];
        [[(StillView *)self.view lastStillLabel] setText:[StringBuilder getLastFeedingString]];
        [[(StillView *)self.view durationLabel] setText:[StringBuilder getLastFeedingDurationString]];
    }
    [[(StillView *)self.view ageLabel] setText:[StringBuilder getAgeString]];
    [[(StillView *)self.view todayLabel] setText:[StringBuilder getFeedingsTodayString]];
}

-(void)updateNoDataImage {
    if ([[StillData getInstance] emptyData]) {
        [[(StillView *)self.view noFeedingImage] setHidden:NO];
    }
    else {
        [[(StillView *)self.view noFeedingImage] setHidden:YES];
    }
}

-(void)updateInterface {
    [self updateLabels];
    [self updateNoDataImage];
    [(StillView *)self.view selectOppositeOfLastUsed];
    [(StillView *)self.view updatePhoto];
    [[(StillView *)self.view timesTableView] reloadData];
    [(StillView *)self.view updateShrinkButtonAlpha];
}


-(BOOL)isTimerRunning {
    return self.timerRunning;
}


-(void)startStopButtonPushed:(UIButton *)sender {
    if (self.timerRunning) { //stop
        [[(StillView *)self.view startStopButton] setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_START_BUTTON] forState:UIControlStateNormal];
        self.timerRunning = NO;
        [[StillData getInstance] insertStillTimeWithStart:self.timerStarted end:[[NSDate date] timeIntervalSince1970] andSide:[self selectedSide]];
        if ([[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:0] count] == 1) {
            [[(StillView *)self.view timesTableView] insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
        }
        else {
            [[(StillView *)self.view timesTableView] insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        }
        [self updateNoDataImage];
        
        self.sideChanged = NO;
        
        [self performSelector:@selector(updateInterface) withObject:nil afterDelay:0.3];
    }
    else { //start
        [[(StillView *)self.view startStopButton] setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_STOP_BUTTON] forState:UIControlStateNormal];
        self.timerRunning = YES;
        self.timerStarted = [[NSDate date] timeIntervalSince1970];
    }
}

-(void)selectSide:(int)side {
    BOOL differs = side != [self selectedSide];
    if (side == SIDE_LEFT) {
        [[(StillView *)self.view lButton] setSelected:YES];
        [[(StillView *)self.view lLabel] setTextColor:[FontProvider getGenderedFontColor]];
        [[(StillView *)self.view rButton] setSelected:NO];
        [[(StillView *)self.view rLabel] setTextColor:[UIColor whiteColor]];
    }
    else if (side == SIDE_RIGHT) {
        [[(StillView *)self.view lButton] setSelected:NO];
        [[(StillView *)self.view lLabel] setTextColor:[UIColor whiteColor]];
        [[(StillView *)self.view rButton] setSelected:YES];
        [[(StillView *)self.view rLabel] setTextColor:[FontProvider getGenderedFontColor]];
    }
    if (self.timerRunning && differs) {
        int currentSide = ([[(StillView *)self.view lButton] isSelected]) ? SIDE_LEFT : SIDE_RIGHT;
        alert = [[CustomAlertView alloc] initWithMessage:NSLocalizedString(@"SideChangeAlertKey", @"") buttonTitles:@[[NSString stringWithFormat:NSLocalizedString(@"SideChangeAlertCancelButtonKey", @""), [[StringBuilder getStringForSide:currentSide] lowercaseString]], NSLocalizedString(@"SideChangeAlertOtherButtonKey", @"")]];
        alert.delegate = self;
        [alert show];
    }
    else if (!self.timerRunning) {
        self.startedSide = side;
    }
}

-(void)buttonPushed:(int)buttonId alertViewTag:(int)tag {
    if (buttonId == 1) { //use both sides
        self.sideChanged = YES;
        [[(StillView *)self.view lButton] setSelected:YES];
        [[(StillView *)self.view lLabel] setTextColor:[FontProvider getGenderedFontColor]];
        [[(StillView *)self.view rButton] setSelected:YES];
        [[(StillView *)self.view rLabel] setTextColor:[FontProvider getGenderedFontColor]];
    }
    else {
        self.sideChanged = NO;
        if ([[(StillView *)self.view rButton] isSelected]) {
            self.startedSide = SIDE_RIGHT;
            [[(StillView *)self.view lButton] setSelected:NO];
            [[(StillView *)self.view lLabel] setTextColor:[UIColor whiteColor]];
        }
        else {
            self.startedSide = SIDE_LEFT;
            [[(StillView *)self.view rButton] setSelected:NO];
            [[(StillView *)self.view rLabel] setTextColor:[UIColor whiteColor]];
        }
    }
}

-(int)selectedSide {
    if (self.sideChanged) {
        if (self.startedSide == SIDE_LEFT) {
            return SIDE_BOTH_LEFT_FIRST;
        }
        else {
            return SIDE_BOTH_RIGHT_FIRST;
        }
    }
    if ([[(StillView *)self.view lButton] isSelected]) {
        return SIDE_LEFT;
    }
    else {
        return SIDE_RIGHT;
    }
}

-(void)showAddEdit:(int)stillTimeId {
    [[NSNotificationCenter defaultCenter] postNotificationName:ADDEDIT_FROM_PULLTOADD_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:stillTimeId] forKey:@"stillTimeId"]];
}


-(void)animateIn {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    [self.view setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    [UIView commitAnimations];
}

-(void)animateOut {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    [self.view setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    [UIView commitAnimations];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}


-(void)shrinkForEdit {
    [(StillView *)self.view shrinkForEdit];
}

-(void)shrinkForEditDone {
    [delegate shrinkForEditDone];
}

-(void)moveUpTopRegion {
    [(StillView *)self.view moveUpTopRegion];
}

-(void)moveUpTopRegionDone {
    [delegate moveUpTopRegionDone];
}

-(void)moveDownTopRegion {
    [(StillView *)self.view moveDownTopRegion];
}

-(void)fadeInTopRegion {
    [(StillView *)self.view fadeInTopRegion];
}

-(void)fadeInTopRegionDone {
    [delegate fadeInTopRegionDone];
}

-(void)fadeOutTopRegion {
    [(StillView *)self.view fadeOutTopRegion];
}

-(void)slideDownList {
    [(StillView *)self.view slideDownList];
}

-(void)slideDownListDone {
    [delegate slideDownListDone];
}

-(void)slideUpList {
    [(StillView *)self.view slideUpList];
}

-(void)slideUpListDone {
    [delegate slideUpListDone];
}

@end
