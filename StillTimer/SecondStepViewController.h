//
//  SecondStepViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <UIKit/UIKit.h>
#import "ThirdStepViewController.h"

@interface SecondStepViewController : UIViewController {
    UIImageView *boyGirlImage;
    
    ThirdStepViewController *controller;
}

@end
