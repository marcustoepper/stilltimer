//
//  ImageProvider.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/13/13.
//
//

#import <Foundation/Foundation.h>

typedef enum {
    IMAGE_PURPOSE_ABORT_BUTTON,
    IMAGE_PURPOSE_ABORT_BUTTON_GREY,
    IMAGE_PURPOSE_BABY_AVATAR_AREA,
    IMAGE_PURPOSE_BABY_AVATAR_AREA_GREY,
    IMAGE_PURPOSE_BABY_BUTTON,
    IMAGE_PURPOSE_BABY_FEED_BUTTON,
    IMAGE_PURPOSE_BABY_SETTINGS_BUTTON,
    IMAGE_PURPOSE_BACKGROUND,
    IMAGE_PURPOSE_BREAST_BUTTON,
    IMAGE_PURPOSE_BREAST_BUTTON_ACTIVE,
    IMAGE_PURPOSE_CALENDAR_ICON,
    IMAGE_PURPOSE_CLOUD_ICON,
    IMAGE_PURPOSE_COLLAPSE_BUTTON,
    IMAGE_PURPOSE_DATE_BUTTON,
    IMAGE_PURPOSE_DATE_BUTTON_ACTIVE,
    IMAGE_PURPOSE_DELETE_BUTTON,
    IMAGE_PURPOSE_EDIT_ICON,
    IMAGE_PURPOSE_EMPTY_DATA_IMAGE,
    IMAGE_PURPOSE_FIELD_BIG,
    IMAGE_PURPOSE_FONT_GRADIENT,
    IMAGE_PURPOSE_LEFT_ARROW_BUTTON,
    IMAGE_PURPOSE_NAME_ICON,
    IMAGE_PURPOSE_NO_PHOTO,
    IMAGE_PURPOSE_NORMAL_BUTTON,
    IMAGE_PURPOSE_PULL_TO_ICON,
    IMAGE_PURPOSE_RIGHT_ARROW_BUTTON,
    IMAGE_PURPOSE_SAVE_BUTTON,
    IMAGE_PURPOSE_SAVE_BUTTON_GREY,
    IMAGE_PURPOSE_SEX_ICON,
    IMAGE_PURPOSE_START_BUTTON,
    IMAGE_PURPOSE_STOP_BUTTON,
    IMAGE_PURPOSE_GENDER_SWITCH_FEMALE,
    IMAGE_PURPOSE_GENDER_SWITCH_MALE,
    IMAGE_PURPOSE_SWITCH_ON,
    IMAGE_PURPOSE_SWITCH_OFF,
    IMAGE_PURPOSE_THREE_ENTRIES_FIELD,
    IMAGE_PURPOSE_TIME_BUTTON,
    IMAGE_PURPOSE_TIME_BUTTON_LEFT_ACTIVE,
    IMAGE_PURPOSE_TIME_BUTTON_RIGHT_ACTIVE,
    IMAGE_PURPOSE_TRASH_ICON,
    
    IMAGE_PURPOSE_PICKER_OVERLAY,
    IMAGE_PURPOSE_PICKER_SEPARATOR
} IMAGE_PURPOSE;

@interface ImageProvider : NSObject

+(UIImage *)getGenderedImageForPurpose:(IMAGE_PURPOSE)purpose;

@end
