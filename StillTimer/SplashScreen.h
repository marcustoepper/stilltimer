//
//  SplashScreen.h
//  stretchtimer
//
//  Created by Alexander Schilling on 5/25/13.
//
//

#import <UIKit/UIKit.h>

@interface SplashScreen : UIViewController

-(void)splash;

@end
