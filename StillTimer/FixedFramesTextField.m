//
//  FixedFramesTextField.m
//  StillTimer
//
//  Created by Alexander Schilling on 6/28/13.
//
//

#import "FixedFramesTextField.h"

@implementation FixedFramesTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    
    return CGRectInset( bounds , 8 , 8 );
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    return CGRectInset( bounds , 8 , 5 );
}

@end
