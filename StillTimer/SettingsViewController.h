//
//  SettingsViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import <UIKit/UIKit.h>
#import "Settings.h"
#import "SettingsView.h"
#import "CustomAlertView.h"

@protocol SettingsViewControllerDelegate <NSObject>

-(void)dismissSettingsView;

-(void)startIntro;

@end

@interface SettingsViewController : UIViewController <SettingsViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CustomAlertViewDelegate> {
    UIImagePickerController *imagePicker;
    
    CustomAlertView *alert;
}

@property (strong, nonatomic) id <SettingsViewControllerDelegate> delegate;

@end
