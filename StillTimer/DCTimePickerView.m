//
//  DVTimePickerView.m
//  StillTimer
//
//  Created by Alexander Schilling on 6/22/13.
//
//

#import "DCTimePickerView.h"

@implementation DCTimePickerView

@synthesize ampm;


//init method
//point: top left point of the picker
- (id)initAtPoint:(CGPoint)point
{
    //create a sample time string for local format
    /* if so. decides to do it right in the future..
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterMediumStyle];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSString *timeString = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:0]];
    
    //12 or 24 hour mode
    self.ampm = NO;
    if ([timeString rangeOfString:@"AM"].location != NSNotFound || [timeString rangeOfString:@"PM"].location != NSNotFound) {
        self.ampm = YES;
    }*/
    
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"de"]) {
        self.ampm = NO;
    }
    else {
        self.ampm = YES;
    }
    
    //hour strings
    NSMutableArray *hours = [[NSMutableArray alloc] init];
    
    int startHours = self.ampm ? 1 : 0;
    int maxHours = self.ampm ? 13 : 24;
    
    //create strings with numbers 0-23 or 1-12
    for (int i=startHours; i<maxHours; i++) {
        NSString *hour = [NSString stringWithFormat:@"%d",i];
        [hours addObject:hour];
    }
    
    //minute strings
    NSMutableArray *mins = [[NSMutableArray alloc] init];
    
    //create strings with numbers 0-59
    for (int i=0; i<10; i++) {
        NSString *min = [NSString stringWithFormat:@"0%d",i];
        [mins addObject:min];
    }
    for (int i=10; i<60; i++) {
        NSString *min = [NSString stringWithFormat:@"%d",i];
        [mins addObject:min];
    }
    
    //create components depending on 12h/24h
    NSArray *components;
    if (self.ampm) {
        components = @[hours, mins, @[@"AM", @"PM"]];
    }
    else {
        components = @[hours, mins];
    }
    
    //initialize with super class init method
    self = [super initAtPoint:point withComponents:components widthMode:DV_PICKER_WIDTH_MODE_VARIABLE];
    
    return self;
}


//scrolls to the current time in the local timezone
-(void)scrollToNowAnimated:(BOOL)animated {
    //call scroll function with current date object
    [self scrollToTime:[NSDate date] animated:animated];
}

//scroll to specified date in the local timezone
-(void)scrollToTime:(NSDate *)time animated:(BOOL)animated {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //get hours in 24h format
    [formatter setDateFormat:@"HH"];
    int hour = [[formatter stringFromDate:time] intValue];
    //change format if necessary
    if (self.ampm) {
        if (hour == 0) {
            [self selectRow:0 inComponent:2 animated:animated];
            hour = 11;
        }
        else if (hour < 12) {
            [self selectRow:0 inComponent:2 animated:animated];
            hour--;
        }
        else if (hour == 12) {
            [self selectRow:1 inComponent:2 animated:animated];
            hour = 11;
        }
        else if (hour > 12) {
            [self selectRow:1 inComponent:2 animated:animated];
            hour -= 13;
        }
    }
    //select correct hour row
    [self selectRow:hour inComponent:0 animated:animated];
    
    //get minutes
    [formatter setDateFormat:@"mm"];
    int min = [[formatter stringFromDate:time] intValue];
    //select correct minute row
    [self selectRow:min inComponent:1 animated:animated];
}

//get the currently selected time
-(NSDate *)getTime {
    //get hour
    int hour = [self getActiveRowForComponent:0];
    //transform 12h to 24h mode
    if (self.ampm) {
        hour++;
        if ([self getActiveRowForComponent:2] == 1 && hour != 12)
            hour += 12;
        if (hour == 12 && [self getActiveRowForComponent:2] == 0)
            hour = 0;
    }
    
    //get minute
    int min = [self getActiveRowForComponent:1];
    
    //create right date object
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit fromDate:[NSDate date]];
    [components setHour:hour];
    [components setMinute:min];
    NSDate *date = [gregorian dateFromComponents:components];
    
    return date;
}

@end
