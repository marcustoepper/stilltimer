//
//  CustomAlertView.m
//  StillTimer
//
//  Created by Alexander Schilling on 9/26/13.
//
//

#import "CustomAlertView.h"

@interface CustomAlertView ()

@end

@implementation CustomAlertView

@synthesize delegate;

@synthesize tag;

- (id)initWithMessage:(NSString *)msg buttonTitles:(NSArray *)buttonTitles
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        int screen_height = [[UIScreen mainScreen] bounds].size.height;
        
        [self.view setFrame:CGRectMake(0, 0, 320, screen_height)];
        [self.view setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.95]];
        
        UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 80, 220, SCREEN_HEIGHT/2-80)];
        [msgLabel setNumberOfLines:0];
        [msgLabel setText:msg];
        [msgLabel setFont:[FontProvider getRegularFontWithSize:20]];
        [msgLabel setTextAlignment:NSTextAlignmentCenter];
        [msgLabel setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
        [msgLabel setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:msgLabel];
        
        for (int i=0; i<[buttonTitles count]; i++) {
            NSString *string = [buttonTitles objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake(50, SCREEN_HEIGHT/2+30+i*60, 221, 45)];
            [button setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_NORMAL_BUTTON] forState:UIControlStateNormal];
            [button setTag:i];
            [button addTarget:self action:@selector(buttonPush:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:button];
            
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, button.frame.size.width, button.frame.size.height-5)];
            [title setText:string];
            [title setFont:[FontProvider getRegularFontWithSize:24]];
            [title setTextAlignment:NSTextAlignmentCenter];
            [title setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
            [title setBackgroundColor:[UIColor clearColor]];
            [button addSubview:title];
        }
    }
    return self;
}


-(void)show {
    [self.view setAlpha:0.0];
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.view];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.view setAlpha:1.0];
    
    [UIView commitAnimations];
}


-(void)buttonPush:(UIButton *)sender {
    if (delegate != nil) {
        [delegate buttonPushed:sender.tag alertViewTag:self.tag];
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(dismiss)];
    
    [self.view setAlpha:0.0];
    
    [UIView commitAnimations];
}


-(void)dismiss {
    [self.view removeFromSuperview];
}

@end
