//
//  AppDelegate.m
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;

@synthesize navigationController;
@synthesize mainController;

@synthesize splash;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.mainController = [[MainViewController alloc] init];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:mainController];
    self.navigationController.navigationBar.hidden = YES;
    
    [self.window setRootViewController:self.navigationController];
    
    self.splash = [[SplashScreen alloc] init];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [self.splash splash];
    
    return YES;
}

@end
