//
//  FontProvider.m
//  StillTimer
//
//  Created by Alexander Schilling on 6/2/13.
//
//

#import "FontProvider.h"

@implementation FontProvider

+(UIFont *)getRegularFontWithSize:(float)size {
    return [UIFont fontWithName:@"MyriadPro-Regular" size:size];
}

+(UIFont *)getBoldFontWithSize:(float)size {
    return [UIFont fontWithName:@"MyriadPro-Bold" size:size];
}

+(UIColor *)getGenderedFontColor {
    if ([[Settings getGender] isEqualToString:GENDER_MALE]) {
        return [UIColor colorWithRed:97.0/255.0 green:172.0/255.0 blue:199.0/255.0 alpha:1.0];
    }
    else {
        return [UIColor colorWithRed:199.0/255.0 green:97.0/255.0 blue:169.0/255.0 alpha:1.0];
    }
}

+(UIColor *)getGenderedFontGradient {
    return [UIColor colorWithPatternImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_FONT_GRADIENT]];
}

@end
