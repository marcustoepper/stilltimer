//
//  FirstStepViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "FirstStepViewController.h"

@interface FirstStepViewController ()

@end

@implementation FirstStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextButton setFrame:CGRectMake(SCREEN_WIDTH-60, 10, 50, 50)];
        [nextButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        UIImageView *nextImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [nextImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_RIGHT_ARROW_BUTTON]];
        [nextButton addSubview:nextImage];
        
        UILabel *momLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 110, 240, 30)];
        [momLabel setText:NSLocalizedString(@"GreetingKey", @"")];
        [momLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:20]];
        [momLabel setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
        [momLabel setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:momLabel];
        
        UILabel *introLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 155, 240, 250)];
        [introLabel setText:NSLocalizedString(@"IntroTextKey", @"")];
        [introLabel setNumberOfLines:0];
        [introLabel setBackgroundColor:[UIColor clearColor]];
        [introLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [introLabel setTextColor:[UIColor colorWithWhite:175.0/255.0 alpha:1.0]];
        [self.view addSubview:introLabel];
        
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-60, SCREEN_WIDTH, 60)];
        [pageControl setNumberOfPages:5];
        [pageControl setCurrentPage:0];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            [pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:205.0/255.0 alpha:1.0]];
            [pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:158.0/255.0 alpha:1.0]];
        }
        [pageControl addTarget:self action:@selector(pageControlTouched:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pageControl];
        
        UISwipeGestureRecognizer *nextRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextStep)];
        [nextRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.view addGestureRecognizer:nextRecognizer];
    }
    return self;
}

-(void)pageControlTouched:(UIPageControl *)pageControl {
    if (pageControl.currentPage > 0) {
        [self nextStep];
    }
    [pageControl setCurrentPage:0];
}

-(void)nextStep {
    controller = [[SecondStepViewController alloc] init];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
