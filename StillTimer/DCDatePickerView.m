//
//  DVDatePickerView.m
//  StillTimer
//
//  Created by Alexander Schilling on 6/18/13.
//
//

#import "DCDatePickerView.h"

@implementation DCDatePickerView

//init method
//point: top left point of the picker
- (id)initAtPoint:(CGPoint)point
{
    //create strings with numbers 1-31
    NSMutableArray *days = [[NSMutableArray alloc] init];
    for (int i=0; i<31; i++) {
        NSString *day = [NSString stringWithFormat:@"%d",i+1];
        [days addObject:day];
    }
    
    //get localized month strings
    NSArray *months = @[NSLocalizedString(@"JanuaryKey", @""), NSLocalizedString(@"FebruaryKey", @""), NSLocalizedString(@"MarchKey", @""), NSLocalizedString(@"AprilKey", @""), NSLocalizedString(@"MayKey", @""), NSLocalizedString(@"JuneKey", @""), NSLocalizedString(@"JulyKey", @""), NSLocalizedString(@"AugustKey", @""), NSLocalizedString(@"SeptemberKey", @""), NSLocalizedString(@"OctoberKey", @""), NSLocalizedString(@"NovemberKey", @""), NSLocalizedString(@"DecemberKey", @"")];
    
    //create year strings starting year through ending year
    NSMutableArray *years = [[NSMutableArray alloc] init];
    for (int i=0; i<DV_DATE_PICKER_ENDING_YEAR-DV_DATE_PICKER_STARTING_YEAR; i++) {
        NSString *year = [NSString stringWithFormat:@"%d",i+DV_DATE_PICKER_STARTING_YEAR];
        [years addObject:year];
    }
    
    //initialize with super class init method
    self = [super initAtPoint:point withComponents:@[days, months, years] widthMode:DV_PICKER_WIDTH_MODE_VARIABLE];
    
    //nothing further to do
    
    return self;
}


//overwrite method from super class to suite the purpose of a date picker
//that is don't select days that do not exist like February 31st
-(void)selectRow:(int)row inComponent:(int)component animated:(BOOL)animated {
    //get the date that will be selected
    int day = [self getActiveRowForComponent:0];
    if (component == 0) day = row;
    int month = [self getActiveRowForComponent:1];
    if (component == 1) month = row;
    int year = [self getActiveRowForComponent:2] + DV_DATE_PICKER_STARTING_YEAR;
    if (component == 2) year = row + DV_DATE_PICKER_STARTING_YEAR;
    
    //determine the number of days for the selected month
    int maxDays = 30; //31 days max by default
    switch (month) {
        case 1: //February
            if (year%4 == 0 && (year%100 != 0 || year%400 == 0)) //leap year
                maxDays = 28;
            else
                maxDays = 27;
            break;
        case 3: //April
        case 5: //June
        case 8: //September
        case 10: //November
            maxDays = 29;
            break;
    }
    
    //if selected day is illegal, go to the nearest possible one
    if (day >= maxDays) [super selectRow:maxDays inComponent:0 animated:YES];
    //otherwise just select the day
    else [super selectRow:day inComponent:0 animated:YES];
    
    //if the selection was not made in the day component, forward the selection
    if (component != 0) [super selectRow:row inComponent:component animated:animated];
    
    //send after animation
    [self performSelector:@selector(delayedDelegateSend) withObject:nil afterDelay:0.3];
}


//! if current year is not in year range, this method won't work
//scrolls to the current day in the local timezone
-(void)scrollToTodayAnimated:(BOOL)animated {
    //forward
    [self scrollToDate:[NSDate date] animated:animated];
}

//! date needs to be in year range, otherwise this method won't work
//scroll to specified date in the local timezone
-(void)scrollToDate:(NSDate *)date animated:(BOOL)animated {
    //format the given date correctly
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    int day = [[formatter stringFromDate:date] intValue];
    [formatter setDateFormat:@"MM"];
    int month = [[formatter stringFromDate:date] intValue];
    [formatter setDateFormat:@"YYYY"];
    int year = [[formatter stringFromDate:date] intValue];
    
    //proof for out of bounds for the year to be selected
    if (year > DV_DATE_PICKER_ENDING_YEAR) year = DV_DATE_PICKER_ENDING_YEAR;
    if (year < DV_DATE_PICKER_STARTING_YEAR) year = DV_DATE_PICKER_STARTING_YEAR;
    
    //select the corresponding rows
    [super selectRow:day-1 inComponent:0 animated:animated];
    [super selectRow:month-1 inComponent:1 animated:animated];
    [super selectRow:year-DV_DATE_PICKER_STARTING_YEAR inComponent:2 animated:animated];
}

//get the currently selected date at 12AM
-(NSDate *)getDate {
    //get the selected rows
    int day = [self getActiveRowForComponent:0] + 1;
    int month = [self getActiveRowForComponent:1] + 1;
    int year = [self getActiveRowForComponent:2] + DV_DATE_PICKER_STARTING_YEAR;
    
    //get the date in the correct time zone
    NSString *dateString = [NSString stringWithFormat:@"%d/%d/%d", month, day, year];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"us"];
    [formatter setLocale:usLocale];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    return [formatter dateFromString:dateString];
}

@end
