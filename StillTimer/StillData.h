//
//  RoomsModel.h
//  myTelco
//
//  Created by Stefan Bayer on 22.06.12.
//  Copyright (c) 2012 deVision coding GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

/*!
 @protocol		StillData
 @abstract		StillData manages the databaseoperations for all still data sets.
 @discussion	Currently, there is nothing to discuss.
 */
@interface StillData : NSObject {
	NSString * databaseName;
	NSString * databasePath;
}

@property (nonatomic, retain) NSString *databaseName;
@property (nonatomic, retain) NSString *databasePath;

@property (nonatomic, strong) NSMutableArray *times;
@property (nonatomic, strong) NSMutableArray *sectionedTimes;

/*!
 @method	getInstance
 @abstract	Returns the singleton-instance.
 @result	StillData *
 */
+(StillData *) getInstance;

/*!
 @method	alloc
 @abstract	Allocates the instance.
 @result	id
 */
+(id) alloc;

/*!
 @method	init
 @abstract	Initializes the instance.
 @result	StillData *
 */
-(StillData *) init;

/*!
 @method	checkAndCreateDatabase
 @abstract	Creates and initializes the database.
 @result	void
 */
-(void) checkAndCreateDatabase;

/*!
 @method	getStillTimes
 @abstract	Select all still times.
 @result	NSMutableArray *
 */
- (NSMutableArray *)getStillTimes;

/*!
 @method	getSectionedStillTimes
 @abstract	Select all still times in an array of arrays by day.
 @result	NSMutableArray *
 */
- (NSMutableArray *)getSectionedStillTimes;

/*!
 @method	insertStillTimeWithStart
 @abstract	Insert a new still time with given start, end and side.
 @result	void
 */
- (void)insertStillTimeWithStart:(double)start end:(double)end andSide:(int)side;

/*!
 @method	deleteStillTimeWithId
 @abstract	delete a still time with given id.
 @result	void
 */
- (void)deleteStillTimeWithId:(int)stillTimeId;

/*!
 @method	updateStillTime
 @abstract	update a still time with given id to given start, end and side.
 @result	void
 */
- (void)updateStillTime:(int)stillTimeId toStart:(double)start end:(double)end andSide:(int)side;

/*!
 @method	getStillTimesCount
 @abstract	get count for all still times.
 @result	int
 */
- (int)getStillTimesCount;

/*!
 @method	getIdWithIndexPath
 @abstract	get the id of an object at indexPath for the linear array.
 @result	int
 */
- (int)getIdWithIndexPath:(NSIndexPath *)indexPath;

-(BOOL)emptyData;

-(void)wipe;

@end
