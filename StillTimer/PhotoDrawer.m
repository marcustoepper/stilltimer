//
//  PhotoDrawer.m
//  StillTimer
//
//  Created by Alexander Schilling on 8/26/13.
//
//

#import "PhotoDrawer.h"

@implementation PhotoDrawer

@synthesize p;
@synthesize scale;
@synthesize image;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = self.frame.size.width/2.0;
        self.layer.masksToBounds = YES;
        
        [self update];
        
        [self setNeedsDisplay];
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    [image drawInRect:CGRectMake(p.x, p.y, image.size.width*scale, image.size.height*scale)];
}


-(void)update {
    image = [Settings getPhoto];
    p = [Settings getPhotoPosition];
    scale = [Settings getPhotoScale];
    
    [self setNeedsDisplay];
}


-(void)applyOffset:(CGPoint)offset {
    p.x = p.x + offset.x;
    if (p.x > 0) p.x = 0;
    if (p.x < 130 - image.size.width*scale) p.x = 130 - image.size.width*scale;
    p.y = p.y + offset.y;
    if (p.y > 0) p.y = 0;
    if (p.y < 130 - image.size.height*scale) p.y = 130 - image.size.height*scale;
    [self setNeedsDisplay];
}


@end
