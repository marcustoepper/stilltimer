//
//  AddEditView.m
//  StillTimer
//
//  Created by Alexander Schilling on 6/21/13.
//
//

#import "AddEditView.h"

@implementation AddEditView

@synthesize delegate;

@synthesize stillTimeId;

@synthesize topRegion;

@synthesize nameLabel;

@synthesize sideButton;
@synthesize sideLabel;

@synthesize dateButton;
@synthesize dateLabel;

@synthesize timeImage;
@synthesize startTimeLabel;
@synthesize endTimeLabel;

@synthesize sidePicker;
@synthesize datePicker;
@synthesize startTimePicker;
@synthesize endTimePicker;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self reloadView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView) name:SETTINGS_CHANGED_NOTIFICATION object:nil];
    }
    return self;
}

-(void)reloadView {
    for (UIView *subview in [self subviews]) {
        [subview removeFromSuperview];
    }
    
    self.topRegion = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.topRegion setAlpha:0.0];
    [self addSubview:self.topRegion];
    
    self.sidePicker = [[DCPickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT-217) withComponents:@[@[NSLocalizedString(@"LeftKey", @""), NSLocalizedString(@"RightKey", @""), NSLocalizedString(@"BothLeftFirstKey", @""), NSLocalizedString(@"BothRightFirstKey", @"")]] widthMode:DV_PICKER_WIDTH_MODE_EQUAL];
    [self.sidePicker setDelegate:self];
    [self.sidePicker setAlpha:0.0];
    [self addSubview:self.sidePicker];
    
    self.datePicker = [[DCDatePickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT)];
    [self.datePicker setDelegate:self];
    [self.sidePicker setAlpha:0.0];
    [self addSubview:self.datePicker];
    
    self.startTimePicker = [[DCTimePickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT)];
    [self.startTimePicker setDelegate:self];
    [self.sidePicker setAlpha:0.0];
    [self addSubview:self.startTimePicker];
    
    self.endTimePicker = [[DCTimePickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT)];
    [self.endTimePicker setDelegate:self];
    [self.sidePicker setAlpha:0.0];
    [self addSubview:self.endTimePicker];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 18+SCREEN_TOP_CORRECTION, SCREEN_WIDTH-120, 40)];
    [self.nameLabel setText:[Settings getName]];
    [self.nameLabel setBackgroundColor:[UIColor clearColor]];
    [self.nameLabel setTextAlignment:NSTextAlignmentCenter];
    [self.nameLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:27.0]];
    [self.nameLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.nameLabel setShadowOffset:CGSizeMake(1, 1)];
    [self.nameLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [self.nameLabel setAdjustsFontSizeToFitWidth:YES];
    [self.topRegion addSubview:self.nameLabel];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(10, 10+SCREEN_TOP_CORRECTION, 50, 50)];
    [cancelButton addTarget:delegate action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setAdjustsImageWhenHighlighted:NO];
    [self.topRegion addSubview:cancelButton];
    
    UIImageView *cancelImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [cancelImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_ABORT_BUTTON]];
    [cancelButton addSubview:cancelImage];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveButton setFrame:CGRectMake(260, 10+SCREEN_TOP_CORRECTION, 50, 50)];
    [saveButton addTarget:delegate action:@selector(saveStillTime:) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setAdjustsImageWhenHighlighted:NO];
    [self.topRegion addSubview:saveButton];
    
    UIImageView *saveImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [saveImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_SAVE_BUTTON]];
    [saveButton addSubview:saveImage];
    
    UILabel *usedBreastLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 97*SCREEN_HEIGHT/548, 120, 30)];
    [usedBreastLabel setText:NSLocalizedString(@"UsedBreastKey", @"")];
    [usedBreastLabel setBackgroundColor:[UIColor clearColor]];
    [usedBreastLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [usedBreastLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [usedBreastLabel setShadowOffset:CGSizeMake(1, 1)];
    [usedBreastLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [usedBreastLabel setAdjustsFontSizeToFitWidth:YES];
    [self.topRegion addSubview:usedBreastLabel];
    
    self.sideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sideButton setFrame:CGRectMake(160, 87*SCREEN_HEIGHT/548, 110, 46)];
    [self.sideButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_DATE_BUTTON] forState:UIControlStateNormal];
    [self.sideButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_DATE_BUTTON_ACTIVE] forState:UIControlStateSelected];
    [self.sideButton addTarget:self action:@selector(sideButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.sideButton setSelected:YES];
    [self.topRegion addSubview:self.sideButton];
    
    self.sideLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 3, 100, 43)];
    [self.sideLabel setBackgroundColor:[UIColor clearColor]];
    [self.sideLabel setTextAlignment:NSTextAlignmentCenter];
    [self.sideLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [self.sideLabel setTextColor:[FontProvider getGenderedFontColor]];
    [self.sideLabel setAdjustsFontSizeToFitWidth:YES];
    [self.sideButton addSubview:self.sideLabel];
    
    UILabel *dateTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 162*SCREEN_HEIGHT/548, 120, 30)];
    [dateTitleLabel setText:NSLocalizedString(@"DateOfFeedingKey", @"")];
    [dateTitleLabel setBackgroundColor:[UIColor clearColor]];
    [dateTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [dateTitleLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [dateTitleLabel setShadowOffset:CGSizeMake(1, 1)];
    [dateTitleLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [dateTitleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.topRegion addSubview:dateTitleLabel];
    
    self.dateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dateButton setFrame:CGRectMake(160, 152*SCREEN_HEIGHT/548, 110, 46)];
    [self.dateButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_DATE_BUTTON] forState:UIControlStateNormal];
    [self.dateButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_DATE_BUTTON_ACTIVE] forState:UIControlStateSelected];
    [self.dateButton addTarget:self action:@selector(dateButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.topRegion addSubview:self.dateButton];
    
    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 3, 100, 43)];
    [self.dateLabel setBackgroundColor:[UIColor clearColor]];
    [self.dateLabel setTextAlignment:NSTextAlignmentCenter];
    [self.dateLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [self.dateLabel setTextColor:[FontProvider getGenderedFontColor]];
    [self.dateLabel setAdjustsFontSizeToFitWidth:YES];
    [self.dateButton addSubview:self.dateLabel];
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 228*SCREEN_HEIGHT/548, 120, 30)];
    [timeLabel setText:NSLocalizedString(@"TimeKey", @"")];
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [timeLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [timeLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [timeLabel setShadowOffset:CGSizeMake(1, 1)];
    [timeLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [timeLabel setAdjustsFontSizeToFitWidth:YES];
    [self.topRegion addSubview:timeLabel];
    
    self.timeImage = [[UIImageView alloc] initWithFrame:CGRectMake(160, 218*SCREEN_HEIGHT/548, 110, 46)];
    [self.timeImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_TIME_BUTTON]];
    [self.timeImage setUserInteractionEnabled:YES];
    [self.topRegion addSubview:self.timeImage];
    
    UIButton *startTimeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [startTimeButton setFrame:CGRectMake(0, 0, 55, 46)];
    [startTimeButton addTarget:self action:@selector(startTimeButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.timeImage addSubview:startTimeButton];
    
    self.startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 3, 47, 43)];
    [self.startTimeLabel setBackgroundColor:[UIColor clearColor]];
    [self.startTimeLabel setTextAlignment:NSTextAlignmentCenter];
    [self.startTimeLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [self.startTimeLabel setTextColor:[FontProvider getGenderedFontColor]];
    [self.startTimeLabel setAdjustsFontSizeToFitWidth:YES];
    [startTimeButton addSubview:self.startTimeLabel];
    
    UIButton *endTimeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [endTimeButton setFrame:CGRectMake(55, 0, 55, 46)];
    [endTimeButton addTarget:self action:@selector(endTimeButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.timeImage addSubview:endTimeButton];
    
    self.endTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 3, 47, 43)];
    [self.endTimeLabel setBackgroundColor:[UIColor clearColor]];
    [self.endTimeLabel setTextAlignment:NSTextAlignmentCenter];
    [self.endTimeLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0]];
    [self.endTimeLabel setTextColor:[FontProvider getGenderedFontColor]];
    [self.endTimeLabel setAdjustsFontSizeToFitWidth:YES];
    [endTimeButton addSubview:self.endTimeLabel];
}

-(void)useStillTimeId:(int)theStillTimeId
{
    self.stillTimeId = theStillTimeId;
    
    if (self.stillTimeId != -1) { //edit
        NSDictionary *currentData = [[[StillData getInstance] getStillTimes] objectAtIndex:self.stillTimeId];
        switch ([[currentData objectForKey:@"side"] intValue]) {
            case SIDE_LEFT:
                [self.sidePicker selectRow:0 inComponent:0 animated:NO];
                break;
            case SIDE_RIGHT:
                [self.sidePicker selectRow:1 inComponent:0 animated:NO];
                break;
            case SIDE_BOTH_LEFT_FIRST:
                [self.sidePicker selectRow:2 inComponent:0 animated:NO];
                break;
            case SIDE_BOTH_RIGHT_FIRST:
                [self.sidePicker selectRow:3 inComponent:0 animated:NO];
                break;
        }
        [self.datePicker scrollToDate:[NSDate dateWithTimeIntervalSince1970:[[currentData objectForKey:@"startTime"] doubleValue]] animated:NO];
        [self.startTimePicker scrollToTime:[NSDate dateWithTimeIntervalSince1970:[[currentData objectForKey:@"startTime"] doubleValue]] animated:NO];
        [self.endTimePicker scrollToTime:[NSDate dateWithTimeIntervalSince1970:[[currentData objectForKey:@"endTime"] doubleValue]] animated:NO];
    }
    else { //add
        [self.sidePicker selectRow:0 inComponent:0 animated:NO];
        [self.datePicker scrollToDate:[NSDate date] animated:NO];
        [self.startTimePicker scrollToTime:[NSDate date] animated:NO];
        [self.endTimePicker scrollToTime:[NSDate date] animated:NO];
    }
    
    [self updateTimeLabels];
    
    [self sideButtonPushed:nil];
}


-(void)pickerView:(DCPickerView *)pickerView didSelectRow:(int)row inComponent:(int)component {
    [self updateTimeLabels];
}


-(void)updateTimeLabels {
    switch ([self.sidePicker getActiveRowForComponent:0]) {
        case 0:
            [self.sideLabel setText:NSLocalizedString(@"LeftKey", @"")];
            break;
        case 1:
            [self.sideLabel setText:NSLocalizedString(@"RightKey", @"")];
            break;
        case 2:
            [self.sideLabel setText:NSLocalizedString(@"BothLeftFirstKey", @"")];
            break;
        case 3:
            [self.sideLabel setText:NSLocalizedString(@"BothRightFirstKey", @"")];
            break;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateLabel setText:[formatter stringFromDate:[self.datePicker getDate]]];
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"de"]) {
        [formatter setDateFormat:@"HH:mm"];
    }
    else {
        [formatter setDateFormat:@"h:mm a"];
    }
    [self.startTimeLabel setText:[formatter stringFromDate:[self.startTimePicker getTime]]];
    [self.endTimeLabel setText:[formatter stringFromDate:[self.endTimePicker getTime]]];
    
    [self.nameLabel setText:[Settings getName]];
}


-(void)sideButtonPushed:(id)sender {
    [self.sideButton setSelected:YES];
    [self.dateButton setSelected:NO];
    [self.timeImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_TIME_BUTTON]];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    
    [self.sidePicker setFrame:CGRectMake(0, SCREEN_HEIGHT-217, 320, 217)];
    [self.datePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.startTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.endTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    
    [UIView commitAnimations];
}


-(void)dateButtonPushed:(id)sender {
    [self.sideButton setSelected:NO];
    [self.dateButton setSelected:YES];
    [self.timeImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_TIME_BUTTON]];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    
    [self.sidePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.datePicker setFrame:CGRectMake(0, SCREEN_HEIGHT-217, 320, 217)];
    [self.startTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.endTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    
    [UIView commitAnimations];
}

-(void)startTimeButtonPushed:(id)sender {
    [self.sideButton setSelected:NO];
    [self.dateButton setSelected:NO];
    [self.timeImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_TIME_BUTTON_LEFT_ACTIVE]];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    
    [self.sidePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.datePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.startTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT-217, 320, 217)];
    [self.endTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    
    [UIView commitAnimations];
}

-(void)endTimeButtonPushed:(id)sender {
    [self.sideButton setSelected:NO];
    [self.dateButton setSelected:NO];
    [self.timeImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_TIME_BUTTON_RIGHT_ACTIVE]];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    
    [self.sidePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.datePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.startTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, 320, 217)];
    [self.endTimePicker setFrame:CGRectMake(0, SCREEN_HEIGHT-217, 320, 217)];
    
    [UIView commitAnimations];
}


-(void)fadeInTopRegion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    [self.topRegion setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)fadeOutTopRegion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    [self.topRegion setAlpha:0.0];
    
    [UIView commitAnimations];
}

-(void)fadeInPicker {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    [self.sidePicker setAlpha:1.0];
    [self.datePicker setAlpha:1.0];
    [self.startTimePicker setAlpha:1.0];
    [self.endTimePicker setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)fadeOutPicker {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(dismiss)];
    
    [self.sidePicker setAlpha:0.0];
    [self.datePicker setAlpha:0.0];
    [self.startTimePicker setAlpha:0.0];
    [self.endTimePicker setAlpha:0.0];
    
    [UIView commitAnimations];
}


-(void)dismiss {
    [self removeFromSuperview];
}

@end
