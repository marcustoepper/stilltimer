//
//  FontProvider.h
//  StillTimer
//
//  Created by Alexander Schilling on 6/2/13.
//
//

#import <Foundation/Foundation.h>

@interface FontProvider : NSObject

+(UIFont *)getRegularFontWithSize:(float)size;
+(UIFont *)getBoldFontWithSize:(float)size;

+(UIColor *)getGenderedFontColor;
+(UIColor *)getGenderedFontGradient;

@end
