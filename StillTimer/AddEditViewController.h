//
//  AddEditViewController.h
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import <UIKit/UIKit.h>
#import "AddEditView.h"
#import "CustomAlertView.h"

@protocol AddEditViewControllerDelegate <NSObject>

-(void)animateEditOut;

@end

@interface AddEditViewController : UIViewController <AddEditViewDelegate> {
    CustomAlertView *alert;
}

@property (strong, nonatomic) id <AddEditViewControllerDelegate> delegate;

@property int stillTimeId;

-(void)useStillTimeId:(int)theStillTimeId;

-(void)fadeInTopRegion;
-(void)fadeOutTopRegion;
-(void)fadeInPicker;
-(void)fadeOutPicker;
-(void)dismiss;

@end
