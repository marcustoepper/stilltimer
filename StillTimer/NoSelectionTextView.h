//
//  NoSelectionTextView.h
//  stretchtimer
//
//  Created by Alexander Schilling on 9/6/13.
//
//

#import <UIKit/UIKit.h>

@interface NoSelectionTextView : UITextView

@end
