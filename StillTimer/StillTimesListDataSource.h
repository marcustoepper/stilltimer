//
//  StillTimesListDataSource.h
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import <Foundation/Foundation.h>
#import "StillTimesListCell.h"

@interface StillTimesListDataSource : NSObject <UITableViewDataSource> {
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
