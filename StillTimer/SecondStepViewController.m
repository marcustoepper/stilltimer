//
//  SecondStepViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "SecondStepViewController.h"

@interface SecondStepViewController ()

@end

@implementation SecondStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [prevButton setFrame:CGRectMake(10, 10, 50, 50)];
        [prevButton addTarget:self action:@selector(previousStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:prevButton];
        
        UIImageView *prevImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [prevImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_LEFT_ARROW_BUTTON]];
        [prevButton addSubview:prevImage];
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextButton setFrame:CGRectMake(SCREEN_WIDTH-60, 10, 50, 50)];
        [nextButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        UIImageView *nextImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [nextImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_RIGHT_ARROW_BUTTON]];
        [nextButton addSubview:nextImage];
        
        UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 90, 240, 60)];
        [questionLabel setText:NSLocalizedString(@"GenderQuestionKey", @"")];
        [questionLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:19]];
        [questionLabel setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
        [questionLabel setBackgroundColor:[UIColor clearColor]];
        [questionLabel setTextAlignment:NSTextAlignmentCenter];
        [questionLabel setNumberOfLines:0];
        [self.view addSubview:questionLabel];
        
        boyGirlImage = [[UIImageView alloc] initWithFrame:CGRectMake(49, 170, 221, 68)];
        [boyGirlImage setUserInteractionEnabled:YES];
        [self setGirl];
        [self.view addSubview:boyGirlImage];
        
        UIButton *girlButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [girlButton setFrame:CGRectMake(0, 0, 111, 68)];
        [girlButton addTarget:self action:@selector(setGirl) forControlEvents:UIControlEventTouchUpInside];
        [boyGirlImage addSubview:girlButton];
        
        UIButton *boyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [boyButton setFrame:CGRectMake(111, 0, 110, 68)];
        [boyButton addTarget:self action:@selector(setBoy) forControlEvents:UIControlEventTouchUpInside];
        [boyGirlImage addSubview:boyButton];
        
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-60, SCREEN_WIDTH, 60)];
        [pageControl setNumberOfPages:5];
        [pageControl setCurrentPage:1];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            [pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:205.0/255.0 alpha:1.0]];
            [pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:158.0/255.0 alpha:1.0]];
        }
        [pageControl addTarget:self action:@selector(pageControlTouched:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pageControl];
        
        UISwipeGestureRecognizer *prevRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(previousStep)];
        [prevRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.view addGestureRecognizer:prevRecognizer];
        
        UISwipeGestureRecognizer *nextRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextStep)];
        [nextRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.view addGestureRecognizer:nextRecognizer];
    }
    return self;
}

-(void)setGirl {
    [boyGirlImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_GENDER_SWITCH_FEMALE]];
    [boyGirlImage setTag:GENDER_FEMALE_ID];
}

-(void)setBoy {
    [boyGirlImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_GENDER_SWITCH_MALE]];
    [boyGirlImage setTag:GENDER_MALE_ID];
}

-(void)pageControlTouched:(UIPageControl *)pageControl {
    if (pageControl.currentPage > 1) {
        [self nextStep];
    }
    else if (pageControl.currentPage < 1) {
        [self previousStep];
    }
    [pageControl setCurrentPage:1];
}

-(void)previousStep {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)nextStep {
    if (boyGirlImage.tag == GENDER_MALE_ID)
        [Settings setGender:GENDER_MALE];
    else
        [Settings setGender:GENDER_FEMALE];
    controller = [[ThirdStepViewController alloc] init];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
