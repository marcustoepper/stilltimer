//
//  FirstStepViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <UIKit/UIKit.h>
#import "SecondStepViewController.h"

@interface FirstStepViewController : UIViewController {
    SecondStepViewController *controller;
}

@end
