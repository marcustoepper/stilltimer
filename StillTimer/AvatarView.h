//
//  AvatarView.h
//  StillTimer
//
//  Created by Alexander Schilling on 8/26/13.
//
//

#import <UIKit/UIKit.h>
#import "PhotoDrawer.h"

@interface AvatarView : UIView

@property (nonatomic, assign) id target;
@property SEL selector;

@property (strong, nonatomic) UIImageView *avatarArea;
@property (strong, nonatomic) PhotoDrawer *drawer;

@property CGPoint refP;

@property BOOL tapMoved;

-(id)initWithCenter:(CGPoint)c andEditable:(BOOL)editable andGendered:(BOOL)gendered;

-(void)setTarget:(id)theTarget selector:(SEL)theSelector;

-(void)update;

@end
