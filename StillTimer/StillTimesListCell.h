//
//  StillTimesListCell.h
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import <UIKit/UIKit.h>
#import "CustomAlertView.h"

@interface StillTimesListCell : UITableViewCell <CustomAlertViewDelegate, UIScrollViewDelegate> {
    CustomAlertView *alert;
}

@property (assign, nonatomic) UITableView *tableView;

@property (strong, nonatomic) UIScrollView *scroll;

@property (strong, nonatomic) NSIndexPath *indexPath;

@property (strong, nonatomic) UILabel *durationLabel;
@property (strong, nonatomic) UILabel *sideLabel;
@property (strong, nonatomic) UILabel *startLabel;
@property (strong, nonatomic) UILabel *endLabel;

@property (strong, nonatomic) UIImageView *deleteImage;
@property (strong, nonatomic) UIImageView *editImage;

@property (strong, nonatomic) UIView *separator;

-(void)reverseAnyAnimation;

@end
