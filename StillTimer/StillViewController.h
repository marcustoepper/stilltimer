//
//  StillViewController.h
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import <UIKit/UIKit.h>
#import "StillView.h"
#import "CustomAlertView.h"

@protocol StillViewControllerDelegate <NSObject>

-(void)shrinkForEditDone;
-(void)moveUpTopRegionDone;
-(void)fadeInTopRegionDone;
-(void)slideUpListDone;
-(void)slideDownListDone;

@end

@interface StillViewController : UIViewController <StillViewDelegate, CustomAlertViewDelegate> {
    CustomAlertView *alert;
}

@property (strong, nonatomic) id <StillViewControllerDelegate> delegate;

@property int startedSide;
@property BOOL sideChanged;

@property BOOL timerRunning;
@property NSTimeInterval timerStarted;

@property (strong, nonatomic) NSTimer *updateInterfaceTimer;

-(void)updateInterface;

-(int)selectedSide;

-(void)animateIn;
-(void)animateOut;

-(void)shrinkForEdit;
-(void)moveUpTopRegion;
-(void)moveDownTopRegion;
-(void)fadeInTopRegion;
-(void)fadeOutTopRegion;
-(void)slideDownList;
-(void)slideUpList;

@end
