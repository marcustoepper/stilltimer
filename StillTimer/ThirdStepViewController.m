//
//  ThirdStepViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "ThirdStepViewController.h"

@interface ThirdStepViewController ()

@end

@implementation ThirdStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [prevButton setFrame:CGRectMake(10, 10, 50, 50)];
        [prevButton addTarget:self action:@selector(previousStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:prevButton];
        
        UIImageView *prevImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [prevImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_LEFT_ARROW_BUTTON]];
        [prevButton addSubview:prevImage];
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextButton setFrame:CGRectMake(SCREEN_WIDTH-60, 10, 50, 50)];
        [nextButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        UIImageView *nextImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [nextImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_RIGHT_ARROW_BUTTON]];
        [nextButton addSubview:nextImage];
        
        UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 90, 240, 60)];
        [questionLabel setText:NSLocalizedString(@"NameQuestionKey", @"")];
        [questionLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:19]];
        [questionLabel setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
        [questionLabel setBackgroundColor:[UIColor clearColor]];
        [questionLabel setTextAlignment:NSTextAlignmentCenter];
        [questionLabel setNumberOfLines:0];
        [self.view addSubview:questionLabel];
        
        UIImageView *nameBg = [[UIImageView alloc] initWithFrame:CGRectMake(49, 170, 221, 68)];
        [nameBg setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_FIELD_BIG]];
        [self.view addSubview:nameBg];
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
            nameField = [[UITextField alloc] initWithFrame:CGRectMake(49, 174, 221, 68)];
        }
        else {
            nameField = [[FixedFramesTextField alloc] initWithFrame:CGRectMake(49, 174, 221, 68)];
        }
        [nameField setText:NSLocalizedString(@"DefaultNameKey", @"")];
        [nameField setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:26]];
        [nameField setTextAlignment:NSTextAlignmentCenter];
        [nameField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [nameField setTextColor:[UIColor colorWithWhite:175.0/255.0 alpha:1.0]];
        [nameField setDelegate:self];
        [self.view addSubview:nameField];
        
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-60, SCREEN_WIDTH, 60)];
        [pageControl setNumberOfPages:5];
        [pageControl setCurrentPage:2];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            [pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:205.0/255.0 alpha:1.0]];
            [pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:158.0/255.0 alpha:1.0]];
        }
        [pageControl addTarget:self action:@selector(pageControlTouched:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pageControl];
        
        UISwipeGestureRecognizer *prevRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(previousStep)];
        [prevRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.view addGestureRecognizer:prevRecognizer];
        
        UISwipeGestureRecognizer *nextRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextStep)];
        [nextRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.view addGestureRecognizer:nextRecognizer];
    }
    return self;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField.text isEqualToString:NSLocalizedString(@"DefaultNameKey", @"")]) {
        [textField setText:@""];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [nameField resignFirstResponder];
    return YES;
}

-(void)pageControlTouched:(UIPageControl *)pageControl {
    if (pageControl.currentPage > 2) {
        [self nextStep];
    }
    else if (pageControl.currentPage < 2) {
        [self previousStep];
    }
    [pageControl setCurrentPage:2];
}

-(void)previousStep {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)nextStep {
    if (![nameField.text isEqualToString:NSLocalizedString(@"DefaultNameKey", @"")] && ![nameField.text isEqualToString:@""] && nameField.text != nil) {
        [Settings setName:nameField.text];
        
        controller = [[FourthStepViewController alloc] init];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    else {
        alert = [[CustomAlertView alloc] initWithMessage:NSLocalizedString(@"EmptyNameImpossibleAlertMessageKey", @"") buttonTitles:@[NSLocalizedString(@"OKKey", @"")]];
        alert.delegate = self;
        [alert show];
    }
}

-(void)buttonPushed:(int)buttonId alertViewTag:(int)tag {
    [nameField becomeFirstResponder];
}

@end
