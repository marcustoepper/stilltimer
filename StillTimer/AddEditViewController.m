//
//  AddEditViewController.m
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import "AddEditViewController.h"

@interface AddEditViewController ()

@end

@implementation AddEditViewController

@synthesize delegate;

@synthesize stillTimeId;


-(void)useStillTimeId:(int)theStillTimeId {
    self.stillTimeId = theStillTimeId;
    [(AddEditView *)self.view useStillTimeId:self.stillTimeId];
}


-(void)loadView {
    self.view = [[AddEditView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
}


-(void)fadeInTopRegion {
    [(AddEditView *)self.view fadeInTopRegion];
}

-(void)fadeOutTopRegion {
    [(AddEditView *)self.view fadeOutTopRegion];
}

-(void)fadeInPicker {
    [(AddEditView *)self.view fadeInPicker];
}

-(void)fadeOutPicker {
    [(AddEditView *)self.view fadeOutPicker];
}

-(void)dismiss {
    [(AddEditView *)self.view dismiss];
}


-(void)animateOut {
    [delegate animateEditOut];
}


-(void)saveStillTime:(id)sender {
    AddEditView *addEdit = (AddEditView *)self.view;
    double dateTimeStart = [[addEdit.datePicker getDate] timeIntervalSince1970]-((int)[[addEdit.datePicker getDate] timeIntervalSince1970]%(60*60*24));
    double dateTimeEnd = [[addEdit.datePicker getDate] timeIntervalSince1970]-((int)[[addEdit.datePicker getDate] timeIntervalSince1970]%(60*60*24));
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ZZZ"];
    int timeshift = [[formatter stringFromDate:[NSDate date]] intValue];
    timeshift /= 100;
    
    double start = ((int)[[addEdit.startTimePicker getTime] timeIntervalSince1970]%(60*60*24));
    double end = ((int)[[addEdit.endTimePicker getTime] timeIntervalSince1970]%(60*60*24));
    
    if (start + timeshift*60*60 >= 24*60*60) {
        dateTimeStart -= 24*60*60;
    }
    else if (start + timeshift*60*60 < 0) {
        dateTimeStart += 24*60*60;
    }
    if (end + timeshift*60*60 >= 24*60*60) {
        dateTimeEnd -= 24*60*60;
    }
    else if (end + timeshift*60*60 < 0) {
        dateTimeEnd += 24*60*60;
    }
    
    double startTime = dateTimeStart+start;
    double endTime = dateTimeEnd+end;
    
    int side = SIDE_LEFT;
    switch ([[(AddEditView *)self.view sidePicker] getActiveRowForComponent:0]) {
        case 0:
            side = SIDE_LEFT;
            break;
        case 1:
            side = SIDE_RIGHT;
            break;
        case 2:
            side = SIDE_BOTH_LEFT_FIRST;
            break;
        case 3:
            side = SIDE_BOTH_RIGHT_FIRST;
            break;
    }
    
    if (endTime < startTime) { //startTime later than endTime
        endTime += 60*60*24;
    }
    if (endTime > [[NSDate date] timeIntervalSince1970]) { //later than now
        alert = [[CustomAlertView alloc] initWithMessage:NSLocalizedString(@"FutureEditAlertMessage", @"") buttonTitles:@[NSLocalizedString(@"FutureEditAlertButtonTitle", @"")]];
        [alert show];
    }
    else {
        if (self.stillTimeId != -1) { //edit
            [[StillData getInstance] updateStillTime:self.stillTimeId toStart:startTime end:endTime andSide:side];
        }
        else { //add
            [[StillData getInstance] insertStillTimeWithStart:startTime end:endTime andSide:side];
            [[NSNotificationCenter defaultCenter] postNotificationName:DATA_UPDATE_NOTIFICATION object:nil];
        }
        [self animateOut];
    }
}

-(void)cancel:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:DATA_UPDATE_NOTIFICATION object:nil];
    [self animateOut];
}

-(void)deleteStillTime:(id)sender {
    [[StillData getInstance] deleteStillTimeWithId:self.stillTimeId];
    [[NSNotificationCenter defaultCenter] postNotificationName:DATA_UPDATE_NOTIFICATION object:nil];
    [self animateOut];
}

@end
