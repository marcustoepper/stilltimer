//
//  FourthStepViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <UIKit/UIKit.h>
#import "FifthStepViewController.h"
#import "DCDatePickerView.h"

@interface FourthStepViewController : UIViewController <DVPickerViewDelegate> {
    UILabel *dateLabel;
    
    DCDatePickerView *datePicker;
    
    FifthStepViewController *controller;
}

@end
