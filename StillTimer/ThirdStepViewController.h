//
//  ThirdStepViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import <UIKit/UIKit.h>
#import "FixedFramesTextField.h"
#import "FourthStepViewController.h"
#import "CustomAlertView.h"

@interface ThirdStepViewController : UIViewController <UITextFieldDelegate, CustomAlertViewDelegate> {
    UITextField *nameField;
    
    CustomAlertView *alert;
    
    FourthStepViewController *controller;
}

@end
