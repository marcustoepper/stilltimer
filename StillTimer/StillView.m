//
//  StillView.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import "StillView.h"

@implementation StillView

@synthesize delegate;

@synthesize bg;

@synthesize stillView;

@synthesize lButton;
@synthesize lLabel;
@synthesize rButton;
@synthesize rLabel;
@synthesize startStopButton;

@synthesize startedLabel;
@synthesize lastStillLabel;
@synthesize lastStillTitleLabel;
@synthesize durationLabel;

@synthesize infoView;

@synthesize avatar;

@synthesize ageLabel;
@synthesize todayLabel;

@synthesize plusImage;
@synthesize pullLabel;
@synthesize noFeedingImage;

@synthesize dataSource;
@synthesize timesTableView;

@synthesize shrinkButton;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        self.dataSource = [[StillTimesListDataSource alloc] init];
        
        [self reloadViews];
    }
    return self;
}


-(void)reloadViews {
    int selectedSide = [delegate selectedSide];
    
    for (UIView *view in [self subviews]) {
        [view removeFromSuperview];
    }
    
    self.timesTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 304, SCREEN_WIDTH, SCREEN_HEIGHT-304)];
    [self.timesTableView setDelegate:self];
    [self.timesTableView setDataSource:dataSource];
    [self.timesTableView setBackgroundColor:[UIColor clearColor]];
    [self.timesTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self addSubview:self.timesTableView];
    [[NSNotificationCenter defaultCenter] addObserver:self.timesTableView selector:@selector(reloadData) name:DATA_UPDATE_NOTIFICATION object:nil];
    
    self.plusImage = [[UIImageView alloc] initWithFrame:CGRectMake(153.5, -31, 15, 15)];
    [self.plusImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_PULL_TO_ICON]];
    [self.timesTableView addSubview:self.plusImage];
    
    self.pullLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -16, SCREEN_WIDTH, 16)];
    [self.pullLabel setText:NSLocalizedString(@"PullToAddKey", @"")];
    [self.pullLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:11]];
    [self.pullLabel setTextColor:[UIColor colorWithWhite:154.0/255.0 alpha:1.0]];
    [self.pullLabel setTextAlignment:NSTextAlignmentCenter];
    [self.pullLabel setBackgroundColor:[UIColor clearColor]];
    [self.timesTableView addSubview:self.pullLabel];
    
    self.noFeedingImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, (self.timesTableView.frame.size.height-164)/2, SCREEN_WIDTH, 164)];
    [self.noFeedingImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_EMPTY_DATA_IMAGE]];
    [self.noFeedingImage setContentMode:UIViewContentModeCenter];
    if ([[StillData getInstance] emptyData])
        [self.noFeedingImage setHidden:NO];
    else
        [self.noFeedingImage setHidden:YES];
    [self.timesTableView addSubview:self.noFeedingImage];
    
    UILabel *notTrackedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 45, SCREEN_WIDTH, 60)];
    [notTrackedLabel setNumberOfLines:2];
    [notTrackedLabel setText:NSLocalizedString(@"NotTrackedKey", @"")];
    [notTrackedLabel setFont:[FontProvider getBoldFontWithSize:20]];
    [notTrackedLabel setTextAlignment:NSTextAlignmentCenter];
    [notTrackedLabel setAdjustsFontSizeToFitWidth:YES];
    [notTrackedLabel setBackgroundColor:[UIColor clearColor]];
    [notTrackedLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [self.noFeedingImage addSubview:notTrackedLabel];
    
    UILabel *toTrackLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 95, SCREEN_WIDTH, 60)];
    [toTrackLabel setText:NSLocalizedString(@"ToTrackKey", @"")];
    [toTrackLabel setFont:[FontProvider getRegularFontWithSize:13]];
    [toTrackLabel setTextAlignment:NSTextAlignmentCenter];
    [toTrackLabel setAdjustsFontSizeToFitWidth:YES];
    [toTrackLabel setBackgroundColor:[UIColor clearColor]];
    [toTrackLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [toTrackLabel setNumberOfLines:0];
    [self.noFeedingImage addSubview:toTrackLabel];
    
    self.bg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 309)];
    [self.bg setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BACKGROUND]];
    [self.bg setUserInteractionEnabled:YES];
    [self addSubview:self.bg];
    
    self.stillView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    [self.bg addSubview:self.stillView];
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [infoButton setFrame:CGRectMake(10, 10+SCREEN_TOP_CORRECTION, 50, 51)];
    [infoButton setAdjustsImageWhenHighlighted:NO];
    [infoButton addTarget:self action:@selector(changeViews) forControlEvents:UIControlEventTouchUpInside];
    [self.stillView addSubview:infoButton];
    
    UIImageView *infoImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 31)];
    [infoImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BABY_BUTTON]];
    [infoButton addSubview:infoImage];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 18+SCREEN_TOP_CORRECTION, SCREEN_WIDTH-120, 40)];
    [nameLabel setText:[Settings getName]];
    [nameLabel setBackgroundColor:[UIColor clearColor]];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    [nameLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:27.0]];
    [nameLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [nameLabel setShadowOffset:CGSizeMake(1, 1)];
    [nameLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [nameLabel setAdjustsFontSizeToFitWidth:YES];
    [self.bg addSubview:nameLabel];
    
    self.lButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.lButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BREAST_BUTTON] forState:UIControlStateNormal];
    [self.lButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BREAST_BUTTON_ACTIVE] forState:UIControlStateSelected];
    [self.lButton setFrame:CGRectMake(0, 104, 90, 92)];
    [self.lButton addTarget:self action:@selector(sideButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.lButton setAdjustsImageWhenHighlighted:NO];
    [self.lButton setSelected:YES];
    [self.lButton setTag:SIDE_LEFT];
    [self.stillView addSubview:self.lButton];
    
    self.lLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 90, 92)];
    [self.lLabel setBackgroundColor:[UIColor clearColor]];
    [self.lLabel setText:NSLocalizedString(@"LeftBreastButtonKey", @"")];
    [self.lLabel setTextColor:[FontProvider getGenderedFontColor]];
    [self.lLabel setTextAlignment:NSTextAlignmentCenter];
    [self.lLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:20]];
    [self.lButton addSubview:self.lLabel];
    
    self.startStopButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.startStopButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_START_BUTTON] forState:UIControlStateNormal];
    if ([delegate isTimerRunning]) {
        [self.startStopButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_STOP_BUTTON] forState:UIControlStateNormal];
    }
    [self.startStopButton setFrame:CGRectMake((SCREEN_WIDTH-138)/2, 80, 138, 139)];
    [self.startStopButton addTarget:delegate action:@selector(startStopButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.startStopButton setAdjustsImageWhenHighlighted:NO];
    [self.stillView addSubview:self.startStopButton];
    
    self.rButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BREAST_BUTTON] forState:UIControlStateNormal];
    [self.rButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BREAST_BUTTON_ACTIVE] forState:UIControlStateSelected];
    [self.rButton setFrame:CGRectMake(228, 104, 90, 92)];
    [self.rButton addTarget:self action:@selector(sideButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.rButton setAdjustsImageWhenHighlighted:NO];
    [self.rButton setTag:SIDE_RIGHT];
    [self.stillView addSubview:self.rButton];
    
    self.rLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 90, 92)];
    [self.rLabel setBackgroundColor:[UIColor clearColor]];
    [self.rLabel setText:NSLocalizedString(@"RightBreastButtonKey", @"")];
    [self.rLabel setTextColor:[UIColor whiteColor]];
    [self.rLabel setTextAlignment:NSTextAlignmentCenter];
    [self.rLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:20]];
    [self.rButton addSubview:self.rLabel];
    
    if (![delegate isTimerRunning]) {
        [self selectOppositeOfLastUsed];
    }
    else {
        if (selectedSide == SIDE_BOTH_LEFT_FIRST || selectedSide == SIDE_BOTH_RIGHT_FIRST) {
            [self.rButton setSelected:YES];
            [self.rLabel setTextColor:[FontProvider getGenderedFontColor]];
        }
        else if (selectedSide == SIDE_RIGHT) {
            [self.lButton setSelected:NO];
            [self.lLabel setTextColor:[UIColor whiteColor]];
            [self.rButton setSelected:YES];
            [self.rLabel setTextColor:[FontProvider getGenderedFontColor]];
        }
    }
    
    self.startedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [self.startedLabel setText:[StringBuilder getLastFeedingStartedString]];
    [self.startedLabel setCenter:CGPointMake(SCREEN_WIDTH/6, 265)];
    [self.startedLabel setTextAlignment:NSTextAlignmentCenter];
    [self.startedLabel setBackgroundColor:[UIColor clearColor]];
    [self.startedLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:14.0]];
    [self.startedLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.startedLabel setShadowOffset:CGSizeMake(1, 1)];
    [self.startedLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [self.startedLabel setAdjustsFontSizeToFitWidth:YES];
    [self.stillView addSubview:self.startedLabel];
    
    UILabel *startedTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [startedTitleLabel setText:NSLocalizedString(@"StartedKey", @"")];
    [startedTitleLabel setCenter:CGPointMake(SCREEN_WIDTH/6, 280)];
    [startedTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [startedTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0]];
    [startedTitleLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [startedTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.stillView addSubview:startedTitleLabel];
    
    self.lastStillLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [self.lastStillLabel setText:[StringBuilder getLastFeedingString]];
    [self.lastStillLabel setCenter:CGPointMake(SCREEN_WIDTH/2, 265)];
    [self.lastStillLabel setTextAlignment:NSTextAlignmentCenter];
    [self.lastStillLabel setBackgroundColor:[UIColor clearColor]];
    [self.lastStillLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:14.0]];
    [self.lastStillLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.lastStillLabel setShadowOffset:CGSizeMake(1, 1)];
    [self.lastStillLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [self.lastStillLabel setAdjustsFontSizeToFitWidth:YES];
    [self.bg addSubview:self.lastStillLabel];
    
    self.lastStillTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [self.lastStillTitleLabel setText:NSLocalizedString(@"LastFeedingKey", @"")];
    [self.lastStillTitleLabel setCenter:CGPointMake(SCREEN_WIDTH/2, 280)];
    [self.lastStillTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.lastStillTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0]];
    [self.lastStillTitleLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.lastStillTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.bg addSubview:self.lastStillTitleLabel];
    
    self.durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [self.durationLabel setText:[StringBuilder getLastFeedingDurationString]];
    [self.durationLabel setCenter:CGPointMake(SCREEN_WIDTH*5/6, 265)];
    [self.durationLabel setTextAlignment:NSTextAlignmentCenter];
    [self.durationLabel setBackgroundColor:[UIColor clearColor]];
    [self.durationLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:14.0]];
    [self.durationLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.durationLabel setShadowOffset:CGSizeMake(1, 1)];
    [self.durationLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [self.durationLabel setAdjustsFontSizeToFitWidth:YES];
    [self.stillView addSubview:self.durationLabel];
    
    UILabel *durationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [durationTitleLabel setText:NSLocalizedString(@"FeedTimeKey", @"")];
    [durationTitleLabel setCenter:CGPointMake(SCREEN_WIDTH*5/6, 280)];
    [durationTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [durationTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0]];
    [durationTitleLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [durationTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.stillView addSubview:durationTitleLabel];
    
    self.infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    [self.infoView setHidden:YES];
    [self.bg addSubview:self.infoView];
    
    UIButton *stillButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [stillButton setFrame:CGRectMake(10, 10+SCREEN_TOP_CORRECTION, 50, 51)];
    [stillButton setAdjustsImageWhenHighlighted:NO];
    [stillButton addTarget:self action:@selector(changeViews) forControlEvents:UIControlEventTouchUpInside];
    [self.infoView addSubview:stillButton];
    
    UIImageView *stillImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 31)];
    [stillImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BABY_FEED_BUTTON]];
    [stillButton addSubview:stillImage];
    
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setFrame:CGRectMake(260, 10+SCREEN_TOP_CORRECTION, 50, 51)];
    [settingsButton setAdjustsImageWhenHighlighted:NO];
    [settingsButton addTarget:delegate action:@selector(animateOut) forControlEvents:UIControlEventTouchUpInside];
    [self.infoView addSubview:settingsButton];
    
    UIImageView *settingsImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 31)];
    [settingsImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_BABY_SETTINGS_BUTTON]];
    [settingsButton addSubview:settingsImage];
    
    self.avatar = [[AvatarView alloc] initWithCenter:CGPointMake(160, 150) andEditable:NO andGendered:YES];
    [self.infoView addSubview:self.avatar];
    
    self.ageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [self.ageLabel setText:[StringBuilder getAgeString]];
    [self.ageLabel setCenter:CGPointMake(SCREEN_WIDTH/6, 265)];
    [self.ageLabel setTextAlignment:NSTextAlignmentCenter];
    [self.ageLabel setBackgroundColor:[UIColor clearColor]];
    [self.ageLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:14.0]];
    [self.ageLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.ageLabel setShadowOffset:CGSizeMake(1, 1)];
    [self.ageLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [self.ageLabel setAdjustsFontSizeToFitWidth:YES];
    [self.infoView addSubview:self.ageLabel];
    
    UILabel *ageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [ageTitleLabel setText:NSLocalizedString(@"AgeKey", @"")];
    [ageTitleLabel setCenter:CGPointMake(SCREEN_WIDTH/6, 280)];
    [ageTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [ageTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0]];
    [ageTitleLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [ageTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.infoView addSubview:ageTitleLabel];
    
    self.todayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [self.todayLabel setText:[StringBuilder getFeedingsTodayString]];
    [self.todayLabel setCenter:CGPointMake(SCREEN_WIDTH*5/6, 265)];
    [self.todayLabel setTextAlignment:NSTextAlignmentCenter];
    [self.todayLabel setBackgroundColor:[UIColor clearColor]];
    [self.todayLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:14.0]];
    [self.todayLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [self.todayLabel setShadowOffset:CGSizeMake(1, 1)];
    [self.todayLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
    [self.todayLabel setAdjustsFontSizeToFitWidth:YES];
    [self.infoView addSubview:self.todayLabel];
    
    UILabel *todayTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [todayTitleLabel setText:NSLocalizedString(@"FeedingsTodayLabelKey", @"")];
    [todayTitleLabel setCenter:CGPointMake(SCREEN_WIDTH*5/6, 280)];
    [todayTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [todayTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0]];
    [todayTitleLabel setTextColor:[FontProvider getGenderedFontGradient]];
    [todayTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.infoView addSubview:todayTitleLabel];
    
    self.shrinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shrinkButton setFrame:CGRectMake(135, 245+SCREEN_TOP_CORRECTION, 50, 51)];
    [self.shrinkButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_COLLAPSE_BUTTON] forState:UIControlStateNormal];
    [self.shrinkButton setAdjustsImageWhenHighlighted:NO];
    [self.shrinkButton setHidden:YES];
    [self.shrinkButton addTarget:self action:@selector(shrink) forControlEvents:UIControlEventTouchUpInside];
    [self.bg addSubview:self.shrinkButton];
    
    UIImageView *shrinkImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 31)];
    [shrinkImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_COLLAPSE_BUTTON]];
    [shrinkButton addSubview:shrinkImage];
}


-(void)updatePhoto {
    [self.avatar update];
}


-(void)changeViews {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    
    if ([self.stillView isHidden]) {
        [self.stillView setHidden:NO];
        [self.infoView setHidden:YES];
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.bg cache:YES];
    }
    else {
        [self.stillView setHidden:YES];
        [self.infoView setHidden:NO];
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.bg cache:YES];
    }
    
    [UIView commitAnimations];
}


-(void)shrinkButtonFallDown {
    [self.shrinkButton setHidden:NO];
    [self.shrinkButton setAlpha:1.0];
    [self.shrinkButton setFrame:CGRectMake(135, 190+SCREEN_TOP_CORRECTION, 50, 51)];
    
    [self shrinkButtonFallDownStep1];
}

-(void)shrinkButtonFallDownStep1 {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shrinkButtonFallDownStep2)];
    
    [self.shrinkButton setFrame:CGRectMake(135, 260, 50, 51)];
    [self.shrinkButton setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)shrinkButtonFallDownStep2 {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shrinkButtonFallDownStep3)];
    
    [self.shrinkButton setFrame:CGRectMake(135, 235+SCREEN_TOP_CORRECTION, 50, 51)];
    [self.shrinkButton setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)shrinkButtonFallDownStep3 {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    [self.shrinkButton setFrame:CGRectMake(135, 245+SCREEN_TOP_CORRECTION, 50, 51)];
    [self.shrinkButton setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)shrinkButtonMoveUp {
    [self.shrinkButton setHidden:YES];
}


-(void)shrinkForEdit {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:delegate];
    [UIView setAnimationDidStopSelector:@selector(shrinkForEditDone)];
    
    [self.timesTableView setFrame:CGRectMake(0, 304, SCREEN_WIDTH, SCREEN_HEIGHT-304)];
    [self.timesTableView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-304) animated:NO];
    [self.bg setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 309)];
    [self.shrinkButton setHidden:YES];
    [self.stillView setAlpha:1.0];
    [self.infoView setAlpha:1.0];
    [self.lastStillLabel setAlpha:1.0];
    [self.lastStillTitleLabel setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)shrink {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(checkFrames)];
    
    [self.timesTableView setFrame:CGRectMake(0, 304, SCREEN_WIDTH, SCREEN_HEIGHT-304)];
    [self.timesTableView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-304) animated:NO];
    [self.bg setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 309)];
    [self.shrinkButton setHidden:YES];
    [self.stillView setAlpha:1.0];
    [self.infoView setAlpha:1.0];
    [self.lastStillLabel setAlpha:1.0];
    [self.lastStillTitleLabel setAlpha:1.0];
    
    [UIView commitAnimations];
}

-(void)checkFrames {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    
    [self.timesTableView setFrame:CGRectMake(0, 304, SCREEN_WIDTH, SCREEN_HEIGHT-304)];
    [self.bg setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 309)];
    
    [UIView commitAnimations];
}

-(void)expand {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shrinkButtonAlphaUp)];
    
    [self.bg setFrame:CGRectMake(0, -239, self.bg.frame.size.width, self.bg.frame.size.height)];
    [self.timesTableView setFrame:CGRectMake(0, 65, SCREEN_WIDTH, SCREEN_HEIGHT - 65)];
    [self.stillView setAlpha:0.0];
    [self.infoView setAlpha:0.0];
    [self.lastStillLabel setAlpha:0.0];
    [self.lastStillTitleLabel setAlpha:0.0];
    
    [UIView commitAnimations];
    
    if ([self.shrinkButton isHidden]) {
        [self shrinkButtonFallDown];
    }
    [self.shrinkButton setAlpha:1.0];
}

-(void)shrinkButtonAlphaUp {
    [self.shrinkButton setAlpha:1.0];
}

-(void)updateShrinkButtonAlpha {
    if (self.bg.frame.origin.y <= -239) {
        [self.shrinkButton setAlpha:1.0];
    }
    else {
        [self.shrinkButton setAlpha:0.0];
    }
}


-(void)moveUpTopRegion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:delegate];
    [UIView setAnimationDidStopSelector:@selector(moveUpTopRegionDone)];
    
    [self.bg setFrame:CGRectMake(0, SCREEN_HEIGHT-521, self.bg.frame.size.width, self.bg.frame.size.height)];
    
    [UIView commitAnimations];
}

-(void)moveDownTopRegion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    [self.bg setFrame:CGRectMake(0, 0, self.bg.frame.size.width, self.bg.frame.size.height)];
    
    [UIView commitAnimations];
}

-(void)fadeInTopRegion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:delegate];
    [UIView setAnimationDidStopSelector:@selector(fadeInTopRegionDone)];
    
    for (UIView *view in [self.bg subviews]) {
        [view setAlpha:1.0];
    }
    
    [UIView commitAnimations];
}

-(void)fadeOutTopRegion {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    
    for (UIView *view in [self.bg subviews]) {
        [view setAlpha:0.0];
    }
    
    [UIView commitAnimations];
}

-(void)slideDownList {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:delegate];
    [UIView setAnimationDidStopSelector:@selector(slideDownListDone)];
    
    [self.timesTableView setFrame:CGRectMake(0, SCREEN_HEIGHT, self.timesTableView.frame.size.width, self.timesTableView.frame.size.height)];
    
    [UIView commitAnimations];
}

-(void)slideUpList {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:ADDEDIT_ANIMATION_DURATION];
    [UIView setAnimationDelegate:delegate];
    [UIView setAnimationDidStopSelector:@selector(slideUpListDone)];
    
    [self.timesTableView setFrame:CGRectMake(0, 304, self.timesTableView.frame.size.width, self.timesTableView.frame.size.height)];
    
    [UIView commitAnimations];
}


-(void)sideButtonPushed:(UIButton *)sender {
    [delegate selectSide:sender.tag];
}

-(void)selectOppositeOfLastUsed {
    if (![delegate isTimerRunning]) {
        if (![[StillData getInstance] emptyData]) {
            int lastUsedSide = [[[[[StillData getInstance] getStillTimes] objectAtIndex:0] objectForKey:@"side"] intValue];
            [delegate selectSide:(lastUsedSide == SIDE_LEFT || lastUsedSide == SIDE_BOTH_RIGHT_FIRST) ? SIDE_RIGHT : SIDE_LEFT];
        }
        else {
            [delegate selectSide:SIDE_LEFT];
        }
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [(StillTimesListDataSource *)tableView.dataSource tableView:tableView viewForHeaderInSection:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [(StillTimesListDataSource *)tableView.dataSource tableView:tableView heightForRowAtIndexPath:indexPath];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int y = scrollView.contentOffset.y;
    float part = (float)(y+15)/(-25);
    if (part > 1) part = 1.0;
    if (part < 0) part = 0.0;
    
    [self.plusImage setTransform:CGAffineTransformMakeRotation(part*M_PI/4)];
    if (scrollView.contentOffset.y < -40) {
        [self.pullLabel setText:NSLocalizedString(@"ReleaseToAddKey", @"")];
    }
    else {
        [self.pullLabel setText:NSLocalizedString(@"PullToAddKey", @"")];
    }
    
    float howFar = 1-(self.timesTableView.frame.origin.y-65)/239.0;
    
    if ((y > 0 && howFar < 1.0) || (y < 0 && howFar > 0.0)) {
        [self.timesTableView setFrame:CGRectMake(0, self.timesTableView.frame.origin.y-y, SCREEN_WIDTH, SCREEN_HEIGHT - self.timesTableView.frame.origin.y + y)];
        [self.timesTableView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, self.timesTableView.frame.size.height) animated:NO];
        [self.stillView setAlpha:(self.timesTableView.frame.origin.y-65)/239.0];
        [self.infoView setAlpha:(self.timesTableView.frame.origin.y-65)/239.0];
        [self.lastStillLabel setAlpha:(self.timesTableView.frame.origin.y-65)/239.0];
        [self.lastStillTitleLabel setAlpha:(self.timesTableView.frame.origin.y-65)/239.0];
        [self.bg setFrame:CGRectOffset(self.bg.frame, 0, -y)];
        [self.shrinkButton setAlpha:5*howFar - 4];
    }
    
    if (self.timesTableView.frame.origin.y < 65 || self.bg.frame.origin.y < -239) {
        [self.bg setFrame:CGRectMake(0, -239, self.bg.frame.size.width, self.bg.frame.size.height)];
        [self.timesTableView setFrame:CGRectMake(0, 65, SCREEN_WIDTH, SCREEN_HEIGHT - 65)];
    }
    else if (self.timesTableView.frame.origin.y > 304 || self.bg.frame.origin.y > 0) {
        [self.bg setFrame:CGRectMake(0, 0, self.bg.frame.size.width, self.bg.frame.size.height)];
        [self.timesTableView setFrame:CGRectMake(0, 304, SCREEN_WIDTH, SCREEN_HEIGHT - 65)];
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView.contentOffset.y < -40) {
        [delegate showAddEdit:-1];
    }
    
    if (!decelerate) {
        float howFar = 1-(self.timesTableView.frame.origin.y-65)/239.0;
        
        if (howFar > 0.0 && howFar < 0.8) {
            [self shrink];
        }
        else if (howFar >= 0.8 && howFar < 1.0) {
            [self expand];
        }
        else if (howFar >= 1.0 && self.shrinkButton.hidden) {
            [self shrinkButtonFallDown];
        }
        else if (howFar >= 1.0) {
            [self.shrinkButton setAlpha:1.0];
        }
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float howFar = 1-(self.timesTableView.frame.origin.y-65)/239.0;
    
    if (howFar > 0.0 && howFar < 0.8) {
        [self shrink];
    }
    else if (howFar >= 0.8 && howFar < 1.0) {
        [self expand];
    }
    else if (howFar >= 1.0 && self.shrinkButton.hidden) {
        [self shrinkButtonFallDown];
    }
    else if (howFar >= 1.0) {
        [self.shrinkButton setAlpha:1.0];
    }
}

@end
