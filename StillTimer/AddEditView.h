//
//  AddEditView.h
//  StillTimer
//
//  Created by Alexander Schilling on 6/21/13.
//
//

#import <UIKit/UIKit.h>
#import "DCDatePickerView.h"
#import "DCTimePickerView.h"

@protocol AddEditViewDelegate <NSObject>

-(void)saveStillTime:(id)sender;
-(void)cancel:(id)sender;
-(void)deleteStillTime:(id)sender;

@end

@interface AddEditView : UIView <DVPickerViewDelegate>

@property (strong, nonatomic) id <AddEditViewDelegate> delegate;

@property int stillTimeId;

@property (strong, nonatomic) UIView *topRegion;

@property (strong, nonatomic) UILabel *nameLabel;

@property (strong, nonatomic) UIButton *sideButton;
@property (strong, nonatomic) UILabel *sideLabel;

@property (strong, nonatomic) UIButton *dateButton;
@property (strong, nonatomic) UILabel *dateLabel;

@property (strong, nonatomic) UIImageView *timeImage;
@property (strong, nonatomic) UILabel *startTimeLabel;
@property (strong, nonatomic) UILabel *endTimeLabel;

@property (strong, nonatomic) DCPickerView *sidePicker;
@property (strong, nonatomic) DCDatePickerView *datePicker;
@property (strong, nonatomic) DCTimePickerView *startTimePicker;
@property (strong, nonatomic) DCTimePickerView *endTimePicker;

-(void)useStillTimeId:(int)theStillTimeId;

-(void)updateTimeLabels;

-(void)sideButtonPushed:(id)sender;
-(void)dateButtonPushed:(id)sender;
-(void)startTimeButtonPushed:(id)sender;
-(void)endTimeButtonPushed:(id)sender;

-(void)fadeInTopRegion;
-(void)fadeOutTopRegion;
-(void)fadeInPicker;
-(void)fadeOutPicker;
-(void)dismiss;

@end
