//
//  ImageProvider.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/13/13.
//
//

#import "ImageProvider.h"

@implementation ImageProvider

+(UIImage *)getGenderedImageForPurpose:(IMAGE_PURPOSE)purpose {
    NSString *imageName = @"";
    switch (purpose) {
        case IMAGE_PURPOSE_ABORT_BUTTON:
            imageName = [NSString stringWithFormat:@"Abort_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_ABORT_BUTTON_GREY:
            imageName = @"Abort_Button_Grey.png";
            break;
        case IMAGE_PURPOSE_BABY_AVATAR_AREA:
            imageName = [NSString stringWithFormat:@"Baby_Avatar_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_BABY_AVATAR_AREA_GREY:
            imageName = @"Baby_Avatar_Settings.png";
            break;
        case IMAGE_PURPOSE_BABY_BUTTON:
            imageName = [NSString stringWithFormat:@"Baby_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_BABY_FEED_BUTTON:
            imageName = [NSString stringWithFormat:@"Baby_Feed_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_BABY_SETTINGS_BUTTON:
            imageName = [NSString stringWithFormat:@"Baby_Settings_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_BACKGROUND:
            imageName = [NSString stringWithFormat:@"Background_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_BREAST_BUTTON:
            imageName = [NSString stringWithFormat:@"Breast_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_BREAST_BUTTON_ACTIVE:
            imageName = [NSString stringWithFormat:@"Breast_Button_%@_Active.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_CALENDAR_ICON:
            imageName = @"Calendar_Icon.png";
            break;
        case IMAGE_PURPOSE_CLOUD_ICON:
            imageName = @"Cloud_Icon.png";
            break;
        case IMAGE_PURPOSE_COLLAPSE_BUTTON:
            imageName = [NSString stringWithFormat:@"Collapse_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_DATE_BUTTON:
            imageName = [NSString stringWithFormat:@"Date_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_DATE_BUTTON_ACTIVE:
            imageName = [NSString stringWithFormat:@"Date_Button_%@_Active.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_DELETE_BUTTON:
            imageName = @"Delete_Button.png";
            break;
        case IMAGE_PURPOSE_EDIT_ICON:
            imageName = [NSString stringWithFormat:@"Edit_Icon_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_EMPTY_DATA_IMAGE:
            imageName = @"Empty_Data_Image.png";
            break;
        case IMAGE_PURPOSE_FIELD_BIG:
            imageName = @"Field_Big.png";
            break;
        case IMAGE_PURPOSE_FONT_GRADIENT:
            if ([Settings getGender] == nil) {
                imageName = @"Gradient_Male.png";
            }
            else {
                imageName = [NSString stringWithFormat:@"Gradient_%@.png", [Settings getGender]];
            }
            break;
        case IMAGE_PURPOSE_LEFT_ARROW_BUTTON:
            imageName = @"Left_Button.png";
            break;
        case IMAGE_PURPOSE_NAME_ICON:
            imageName = @"Name_Icon.png";
            break;
        case IMAGE_PURPOSE_NO_PHOTO:
            imageName = @"No_Photo.png";
            break;
        case IMAGE_PURPOSE_NORMAL_BUTTON:
            imageName = @"Normal_Button.png";
            break;
        case IMAGE_PURPOSE_PULL_TO_ICON:
            imageName = @"Pull_To_Icon.png";
            break;
        case IMAGE_PURPOSE_RIGHT_ARROW_BUTTON:
            imageName = @"Right_Button.png";
            break;
        case IMAGE_PURPOSE_SAVE_BUTTON:
            imageName = [NSString stringWithFormat:@"Save_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_SAVE_BUTTON_GREY:
            imageName = @"Save_Button_Grey.png";
            break;
        case IMAGE_PURPOSE_SEX_ICON:
            imageName = @"Sex_Icon.png";
            break;
        case IMAGE_PURPOSE_START_BUTTON:
            imageName = [NSString stringWithFormat:@"Start_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_STOP_BUTTON:
            imageName = [NSString stringWithFormat:@"Stop_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_GENDER_SWITCH_FEMALE:
            imageName = @"Switch_Button_Female.png";
            break;
        case IMAGE_PURPOSE_GENDER_SWITCH_MALE:
            imageName = @"Switch_Button_Male.png";
            break;
        case IMAGE_PURPOSE_SWITCH_OFF:
            imageName = @"Switch_Off.png";
            break;
        case IMAGE_PURPOSE_SWITCH_ON:
            imageName = @"Switch_On.png";
            break;
        case IMAGE_PURPOSE_THREE_ENTRIES_FIELD:
            imageName = @"Three_Entries_Field.png";
            break;
        case IMAGE_PURPOSE_TIME_BUTTON:
            imageName = [NSString stringWithFormat:@"Time_Button_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_TIME_BUTTON_LEFT_ACTIVE:
            imageName = [NSString stringWithFormat:@"Time_Button_%@_LeftActive.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_TIME_BUTTON_RIGHT_ACTIVE:
            imageName = [NSString stringWithFormat:@"Time_Button_%@_RightActive.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_TRASH_ICON:
            imageName = [NSString stringWithFormat:@"Trash_Icon_%@.png", [Settings getGender]];
            break;
            
        case IMAGE_PURPOSE_PICKER_OVERLAY:
            imageName = [NSString stringWithFormat:@"overlay_%@.png", [Settings getGender]];
            break;
        case IMAGE_PURPOSE_PICKER_SEPARATOR:
            imageName = [NSString stringWithFormat:@"separator_%@.png", [Settings getGender]];
            break;
    }
    return [UIImage imageNamed:imageName];
}

@end
