//
//  AppDelegate.h
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "SplashScreen.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UINavigationController *navigationController;
    MainViewController *mainController;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) MainViewController *mainController;

@property (strong, nonatomic) SplashScreen *splash;

@end
