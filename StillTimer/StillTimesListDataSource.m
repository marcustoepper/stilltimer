//
//  StillTimesListDataSource.m
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#import "StillTimesListDataSource.h"

@implementation StillTimesListDataSource


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[[StillData getInstance] getSectionedStillTimes] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    StillTimesListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[StillTimesListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell reverseAnyAnimation];
    
    cell.tableView = tableView;
    
    NSDictionary *data = [[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    double startTime = [[data objectForKey:@"startTime"] doubleValue];
    double endTime = [[data objectForKey:@"endTime"] doubleValue];
    int side = [[data objectForKey:@"side"] intValue];
    double duration = endTime-startTime;
    
    cell.sideLabel.text = [StringBuilder getStringForSide:side];
    cell.startLabel.text = [StringBuilder getStringForTime:startTime];
    cell.endLabel.text = [StringBuilder getStringForTime:endTime];
    cell.durationLabel.text = [StringBuilder getStringForDuration:duration];
    
    cell.indexPath = indexPath;
    
    if (indexPath.row == [[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:indexPath.section] count]-1 &&
        indexPath.section != [[[StillData getInstance] getSectionedStillTimes] count]-1) {
        [cell.separator setHidden:YES];
    }
    else {
        [cell.separator setHidden:NO];
    }
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [headerView setBackgroundColor:[UIColor colorWithWhite:229.0/255.0 alpha:1.0]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 1, 120, 23)];
    [dateLabel setBackgroundColor:[UIColor clearColor]];
    [dateLabel setTextColor:[UIColor colorWithWhite:154.0/255.0 alpha:1.0]];
    [dateLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12]];
    [dateLabel setText:[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[[[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:section] objectAtIndex:0] objectForKey:@"startTime"] doubleValue]]]];
    [headerView addSubview:dateLabel];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

@end
