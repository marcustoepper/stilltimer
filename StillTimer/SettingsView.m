//
//  SettingsView.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import "SettingsView.h"

@implementation SettingsView

@synthesize delegate;

@synthesize avatar;

@synthesize settingsScroll;

@synthesize backButton;

@synthesize nameField;
@synthesize birthdayButton;
@synthesize birthdayLabel;
@synthesize birthdayPicker;
@synthesize genderButton;
@synthesize genderLabel;
@synthesize genderPicker;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:234.0/255.0 alpha:1.0]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadViews) name:SETTINGS_CHANGED_NOTIFICATION object:nil];
        
        [self reloadViews];
    }
    return self;
}

-(void)reloadViews {
    for (UIView *view in [self subviews]) {
        [view removeFromSuperview];
    }
    
    UILabel *settingsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 23+SCREEN_TOP_CORRECTION, SCREEN_WIDTH, 30)];
    [settingsLabel setText:NSLocalizedString(@"SettingsKey", @"")];
    [settingsLabel setBackgroundColor:[UIColor clearColor]];
    [settingsLabel setTextAlignment:NSTextAlignmentCenter];
    [settingsLabel setFont:[FontProvider getBoldFontWithSize:24]];
    [settingsLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [self addSubview:settingsLabel];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(10, 10+SCREEN_TOP_CORRECTION, 50, 50)];
    [cancelButton addTarget:delegate action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setAdjustsImageWhenHighlighted:NO];
    [self addSubview:cancelButton];
    
    UIImageView *cancelImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [cancelImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_ABORT_BUTTON_GREY]];
    [cancelButton addSubview:cancelImage];
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [confirmButton setFrame:CGRectMake(260, 10+SCREEN_TOP_CORRECTION, 50, 50)];
    [confirmButton addTarget:delegate action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [confirmButton setAdjustsImageWhenHighlighted:NO];
    [self addSubview:confirmButton];
    
    UIImageView *confirmImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [confirmImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_SAVE_BUTTON_GREY]];
    [confirmButton addSubview:confirmImage];
    
    self.avatar = [[AvatarView alloc] initWithCenter:CGPointMake(160, 133.5+SCREEN_TOP_CORRECTION) andEditable:YES andGendered:NO];
    [self.avatar setTarget:self selector:@selector(photoActionSheetDisplay)];
    [self addSubview:self.avatar];
    
    self.settingsScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 220+SCREEN_TOP_CORRECTION, SCREEN_WIDTH, SCREEN_HEIGHT-220-SCREEN_TOP_CORRECTION)];
    if (SCREEN_HEIGHT <= 480) {
        [self.settingsScroll setContentSize:CGSizeMake(SCREEN_WIDTH, 220+SCREEN_TOP_CORRECTION)];
    }
    else {
        [self.settingsScroll setContentSize:CGSizeMake(SCREEN_WIDTH, 270+SCREEN_TOP_CORRECTION)];
    }
    [self addSubview:self.settingsScroll];
    
    UILabel *informationLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 0, 200, 20)];
    [informationLabel setText:NSLocalizedString(@"BabyInformationKey", @"")];
    [informationLabel setBackgroundColor:[UIColor clearColor]];
    [informationLabel setFont:[FontProvider getRegularFontWithSize:14]];
    [informationLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [self.settingsScroll addSubview:informationLabel];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setFrame:CGRectMake(0, 0, self.settingsScroll.contentSize.width, self.settingsScroll.contentSize.height)];
    [self.backButton addTarget:self action:@selector(everythingOut) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setHidden:YES];
    [self.settingsScroll addSubview:self.backButton];
    
    UIImageView *threeEntriesField = [[UIImageView alloc] initWithFrame:CGRectMake(50, 20, 221, 136)];
    [threeEntriesField setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_THREE_ENTRIES_FIELD]];
    [self.settingsScroll addSubview:threeEntriesField];
    
    UIImageView *nameIcon = [[UIImageView alloc] initWithFrame:CGRectMake(63, 37, 13, 11)];
    [nameIcon setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_NAME_ICON]];
    [self.settingsScroll addSubview:nameIcon];
    
    self.nameField = [[UITextField alloc] initWithFrame:CGRectMake(90, 36, 170, 20)];
    [self.nameField setDelegate:self];
    [self.nameField setText:[Settings getName]];
    [self.nameField setBackgroundColor:[UIColor clearColor]];
    [self.nameField setFont:[FontProvider getRegularFontWithSize:14]];
    [self.nameField setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [self.settingsScroll addSubview:self.nameField];
    
    UIImageView *birthdayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(63, 81, 12, 13)];
    [birthdayIcon setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_CALENDAR_ICON]];
    [self.settingsScroll addSubview:birthdayIcon];
    
    self.birthdayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.birthdayButton setFrame:CGRectMake(50, 66, 220, 44)];
    [self.birthdayButton addTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
    [self.settingsScroll addSubview:self.birthdayButton];
    
    self.birthdayLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 14, 170, 20)];
    [self.birthdayLabel setBackgroundColor:[UIColor clearColor]];
    [self.birthdayLabel setFont:[FontProvider getRegularFontWithSize:14]];
    [self.birthdayLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [self.birthdayButton addSubview:self.birthdayLabel];
    
    UIImageView *genderIcon = [[UIImageView alloc] initWithFrame:CGRectMake(63, 125, 13, 17)];
    [genderIcon setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_SEX_ICON]];
    [self.settingsScroll addSubview:genderIcon];
    
    self.genderButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.genderButton setFrame:CGRectMake(50, 111, 220, 44)];
    [self.genderButton addTarget:self action:@selector(animateGenderPickerIn:) forControlEvents:UIControlEventTouchUpInside];
    [self.settingsScroll addSubview:self.genderButton];
    
    self.genderLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 14, 170, 20)];
    [self.genderLabel setBackgroundColor:[UIColor clearColor]];
    [self.genderLabel setFont:[FontProvider getRegularFontWithSize:14]];
    [self.genderLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
    [self.genderButton addSubview:self.genderLabel];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton setFrame:CGRectMake(50, 175, 221, 45)];
    [deleteButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_DELETE_BUTTON] forState:UIControlStateNormal];
    [deleteButton addTarget:delegate action:@selector(deleteEverything) forControlEvents:UIControlEventTouchUpInside];
    [self.settingsScroll addSubview:deleteButton];
    
    UILabel *deleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 221, 40)];
    [deleteLabel setText:NSLocalizedString(@"DeleteDataKey", @"")];
    [deleteLabel setBackgroundColor:[UIColor clearColor]];
    [deleteLabel setTextAlignment:NSTextAlignmentCenter];
    [deleteLabel setTextColor:[UIColor whiteColor]];
    [deleteLabel setFont:[FontProvider getRegularFontWithSize:14]];
    [deleteButton addSubview:deleteLabel];
    
    UIButton *impressumButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [impressumButton setImage:[UIImage imageNamed:@"impressum.png"] forState:UIControlStateNormal];
    [impressumButton setContentMode:UIViewContentModeCenter];
    [impressumButton setAdjustsImageWhenHighlighted:NO];
    [impressumButton setBackgroundColor:[UIColor clearColor]];
    [impressumButton addTarget:self action:@selector(toImpressumAction) forControlEvents:UIControlEventTouchUpInside];
    if (SCREEN_HEIGHT <= 480) {
        [impressumButton setFrame:CGRectMake(270, 410, 50, 50)];
        [self addSubview:impressumButton];
    }
    else {
        [impressumButton setFrame:CGRectMake(50, 220, 220, 50)];
        [self.settingsScroll addSubview:impressumButton];
    }
    
    self.birthdayPicker = [[DCDatePickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT)];
    [self.birthdayPicker setDelegate:self];
    [self.birthdayPicker scrollToDate:[Settings getBirthday] animated:NO];
    [self addSubview:self.birthdayPicker];
    
    self.genderPicker = [[DCPickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT) withComponents:@[@[NSLocalizedString(@"BoyKey", @""),NSLocalizedString(@"GirlKey", @"")]] widthMode:DV_PICKER_WIDTH_MODE_EQUAL];
    [self.genderPicker setDelegate:self];
    if ([[Settings getGender] isEqualToString:GENDER_MALE])
        [self.genderPicker selectRow:0 inComponent:0 animated:NO];
    else
        [self.genderPicker selectRow:1 inComponent:0 animated:NO];
    [self addSubview:self.genderPicker];
    
    [self updateDateLabel];
    [self updateGenderLabel];
}


-(void)toImpressumAction {
    ImprintViewController *imprint = [[ImprintViewController alloc] init];
    [(UINavigationController *)[[UIApplication sharedApplication] keyWindow].rootViewController pushViewController:imprint animated:YES];
}


-(void)shrinkSettingsScroll {
    [self.avatar setFrame:CGRectMake(86.5, -150+SCREEN_TOP_CORRECTION, 147, 147)];
    [self.settingsScroll setFrame:CGRectMake(0, 60+SCREEN_TOP_CORRECTION, SCREEN_WIDTH, SCREEN_HEIGHT-80-self.birthdayPicker.frame.size.height-SCREEN_TOP_CORRECTION)];
    
    [self.settingsScroll setScrollEnabled:NO];
    
    [self.backButton setHidden:NO];
}

-(void)resetSettingsScroll {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [self.avatar setFrame:CGRectMake(86.5, 60+SCREEN_TOP_CORRECTION, 147, 147)];
    [self.settingsScroll setFrame:CGRectMake(0, 220+SCREEN_TOP_CORRECTION, SCREEN_WIDTH, SCREEN_HEIGHT-220-SCREEN_TOP_CORRECTION)];
    
    [UIView commitAnimations];
    
    [self.settingsScroll setScrollEnabled:YES];
    
    [self.backButton setHidden:YES];
}


-(void)photoActionSheetDisplay {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"CancelKey", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"PhotoLibraryKey", @""), NSLocalizedString(@"PhotoCameraKey", @""), nil];
        [sheet showInView:self];
    }
    else {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"CancelKey", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"PhotoLibraryKey", @""), nil];
        [sheet showInView:self];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [delegate choosePhotoFromLibrary];
    }
    else if (buttonIndex == 1) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [delegate takePhoto];
        }
    }
}


-(void)animateDatePickerIn:(UIButton *)sender {
    [self genderPickerOut];
    
    [sender removeTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
    [sender addTarget:self action:@selector(animateDatePickerOut:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.birthdayPicker setFrame:CGRectMake(0, SCREEN_HEIGHT-self.birthdayPicker.frame.size.height, SCREEN_WIDTH, self.birthdayPicker.frame.size.height)];
    
    [UIView commitAnimations];
    
    [self shrinkSettingsScroll];
    
    [self.nameField resignFirstResponder];
}

-(void)updateDateLabel {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *date = [self.birthdayPicker getDate];
    
    if ([[formatter stringFromDate:date] isEqualToString:[formatter stringFromDate:[NSDate date]]]) {
        [self.birthdayLabel setText:NSLocalizedString(@"TodayKey", @"")];
    }
    else if ([[formatter stringFromDate:date] isEqualToString:[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970] - (24*60*60)]]]) {
        [self.birthdayLabel setText:NSLocalizedString(@"YesterdayKey", @"")];
    }
    else {
        [self.birthdayLabel setText:[[formatter stringFromDate:date] lowercaseString]];
    }
}

-(void)animateDatePickerOut:(UIButton *)sender {
    [sender removeTarget:self action:@selector(animateDatePickerOut:) forControlEvents:UIControlEventTouchUpInside];
    [sender addTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.birthdayPicker setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, self.birthdayPicker.frame.size.height)];
    
    [UIView commitAnimations];
    
    [self resetSettingsScroll];
}


-(void)animateGenderPickerIn:(UIButton *)sender {
    [self birthdayPickerOut];
    
    [sender removeTarget:self action:@selector(animateGenderPickerIn:) forControlEvents:UIControlEventTouchUpInside];
    [sender addTarget:self action:@selector(animateGenderPickerOut:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.genderPicker setFrame:CGRectMake(0, SCREEN_HEIGHT-self.genderPicker.frame.size.height, SCREEN_WIDTH, self.genderPicker.frame.size.height)];
    
    [UIView commitAnimations];
    
    [self shrinkSettingsScroll];
    
    [self.nameField resignFirstResponder];
}

-(void)updateGenderLabel {
    if ([self.genderPicker getActiveRowForComponent:0] == 0) {
        [self.genderLabel setText:NSLocalizedString(@"BoyKey", @"")];
    }
    else {
        [self.genderLabel setText:NSLocalizedString(@"GirlKey", @"")];
    }
}

-(void)animateGenderPickerOut:(UIButton *)sender {
    [sender removeTarget:self action:@selector(animateGenderPickerOut:) forControlEvents:UIControlEventTouchUpInside];
    [sender addTarget:self action:@selector(animateGenderPickerIn:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.genderPicker setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, self.genderPicker.frame.size.height)];
    
    [UIView commitAnimations];
    
    [self resetSettingsScroll];
}


-(void)pickerOut {
    [self birthdayPickerOut];
    [self genderPickerOut];
}

-(void)birthdayPickerOut {
    [self.birthdayButton removeTarget:self action:@selector(animateDatePickerOut:) forControlEvents:UIControlEventTouchUpInside];
    [self.birthdayButton addTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.birthdayPicker setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, self.birthdayPicker.frame.size.height)];
    
    [UIView commitAnimations];
}

-(void)genderPickerOut {
    [self.genderButton removeTarget:self action:@selector(animateGenderPickerOut:) forControlEvents:UIControlEventTouchUpInside];
    [self.genderButton addTarget:self action:@selector(animateGenderPickerIn:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [self.genderPicker setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, self.genderPicker.frame.size.height)];
    
    [UIView commitAnimations];
}


-(void)pickerView:(DCPickerView *)pickerView didSelectRow:(int)row inComponent:(int)component {
    [self updateDateLabel];
    [self updateGenderLabel];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self shrinkSettingsScroll];
    [self pickerOut];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self resetSettingsScroll];
    return YES;
}


-(void)everythingOut {
    [self resetSettingsScroll];
    [self.nameField resignFirstResponder];
    [self pickerOut];
}

@end
