//
//  Defines.h
//  StillTimer
//
//  Created by Alexander on 03.12.12.
//
//

#ifndef StillTimer_Defines_h
#define StillTimer_Defines_h

#define SIDE_LEFT 1
#define SIDE_RIGHT 2
#define SIDE_BOTH_LEFT_FIRST 3
#define SIDE_BOTH_RIGHT_FIRST 4

#define DATA_UPDATE_NOTIFICATION @"TABLES_UPDATED"
#define SETTINGS_CHANGED_NOTIFICATION @"SETTINGS_CHANGED"

#define ADDEDIT_FROM_PULLTOADD_NOTIFICATION @"ADDEDIT_PULLTOADD"
#define ADDEDIT_FROM_LISTSMALL_NOTIFICATION @"ADDEDIT_LISTSMALL"
#define ADDEDIT_FROM_LISTLARGE_NOTIFICATION @"ADDEDIT_LISTLARGE"

#define ADDEDIT_ANIMATION_DURATION 0.4

#define CELL_PULL_DISTANCE 58

#define SCREEN_RECT [[UIScreen mainScreen] bounds]
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) ? [[UIScreen mainScreen] bounds].size.height : ([[UIScreen mainScreen] bounds].size.height-20))
#define SCREEN_TOP_CORRECTION (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) ? 10 : 0)

#define GENDER_MALE @"Male"
#define GENDER_FEMALE @"Female"

#define GENDER_MALE_ID 1
#define GENDER_FEMALE_ID 2

#define BABY_NAME_KEY @"BABY_NAME"
#define BABY_BIRTHDAY_KEY @"BABY_BIRTHDAY"
#define BABY_GENDER_KEY @"BABY_GENDER"
#define BABY_PHOTO_FILE_NAME @"baby_photo.png"
#define BABY_PHOTO_POSITION_X_KEY @"BABY_PHOTO_POSITION_X"
#define BABY_PHOTO_POSITION_Y_KEY @"BABY_PHOTO_POSITION_Y"
#define BABY_PHOTO_SCALE_KEY @"BABY_PHOTO_SCALE"

#endif
