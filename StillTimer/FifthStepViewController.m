//
//  FifthStepViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "FifthStepViewController.h"

@interface FifthStepViewController ()

@end

@implementation FifthStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [prevButton setFrame:CGRectMake(10, 10, 50, 50)];
        [prevButton addTarget:self action:@selector(previousStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:prevButton];
        
        UIImageView *prevImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [prevImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_LEFT_ARROW_BUTTON]];
        [prevButton addSubview:prevImage];
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextButton setFrame:CGRectMake(SCREEN_WIDTH-60, 10, 50, 50)];
        [nextButton addTarget:self action:@selector(finishUp) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        UIImageView *nextImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [nextImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_SAVE_BUTTON_GREY]];
        [nextButton addSubview:nextImage];
        
        UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 240, 60)];
        [questionLabel setText:NSLocalizedString(@"PhotoQuestionKey", @"")];
        [questionLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:19]];
        [questionLabel setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
        [questionLabel setBackgroundColor:[UIColor clearColor]];
        [questionLabel setTextAlignment:NSTextAlignmentCenter];
        [questionLabel setNumberOfLines:0];
        [self.view addSubview:questionLabel];
        
        avatar = [[AvatarView alloc] initWithCenter:CGPointMake(160, 173.5) andEditable:YES andGendered:NO];
        [self.view addSubview:avatar];
        
        UIButton *libraryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [libraryButton setFrame:CGRectMake(49, SCREEN_HEIGHT*0.6-10, 221, 68)];
        [libraryButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_FIELD_BIG] forState:UIControlStateNormal];
        [libraryButton addTarget:self action:@selector(choosePhotoFromLibrary) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:libraryButton];
        
        UILabel *libraryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 221, 68)];
        [libraryLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [libraryLabel setTextAlignment:NSTextAlignmentCenter];
        [libraryLabel setTextColor:[UIColor colorWithWhite:175.0/255.0 alpha:1.0]];
        [libraryLabel setUserInteractionEnabled:NO];
        [libraryLabel setBackgroundColor:[UIColor clearColor]];
        [libraryLabel setText:NSLocalizedString(@"PhotoLibraryKey", @"")];
        [libraryButton addSubview:libraryLabel];
        
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-60, SCREEN_WIDTH, 60)];
        [pageControl setNumberOfPages:5];
        [pageControl setCurrentPage:4];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            [pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:205.0/255.0 alpha:1.0]];
            [pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:158.0/255.0 alpha:1.0]];
        }
        [pageControl addTarget:self action:@selector(pageControlTouched:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pageControl];
        
        UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cameraButton setFrame:CGRectMake(49, SCREEN_HEIGHT*0.75-5, 221, 68)];
        [cameraButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_FIELD_BIG] forState:UIControlStateNormal];
        [cameraButton addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [self.view addSubview:cameraButton];
        }
        
        UILabel *cameraLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 221, 68)];
        [cameraLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [cameraLabel setTextAlignment:NSTextAlignmentCenter];
        [cameraLabel setTextColor:[UIColor colorWithWhite:175.0/255.0 alpha:1.0]];
        [cameraLabel setUserInteractionEnabled:NO];
        [cameraLabel setBackgroundColor:[UIColor clearColor]];
        [cameraLabel setText:NSLocalizedString(@"PhotoCameraKey", @"")];
        [cameraButton addSubview:cameraLabel];
        
        UISwipeGestureRecognizer *prevRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(previousStep)];
        [prevRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.view addGestureRecognizer:prevRecognizer];
    }
    return self;
}

-(void)choosePhotoFromLibrary {
    imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)takePhoto {
    imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    /*[imagePicker setShowsCameraControls:NO];
    [imagePicker setHidesBottomBarWhenPushed:YES];
    int realScreenHeight = 0;
    int pixelsToFill = 54;
    UIImageView *mask;
    if (SCREEN_HEIGHT == 460) {
        realScreenHeight = 480;
        mask = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 480)] autorelease];
        [mask setImage:[UIImage imageNamed:@"Photomask_small.png"]];
    }
    else {
        realScreenHeight = 568;
        mask = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 568)] autorelease];
        [mask setImage:[UIImage imageNamed:@"Photomask_big.png"]];
    }
    [mask setUserInteractionEnabled:YES];
    
    UIButton *takePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [takePhotoButton setFrame:CGRectMake((SCREEN_WIDTH-58)/2, realScreenHeight-58-20, 58, 58)];
    [takePhotoButton setImage:[UIImage imageNamed:@"Snap_Button.png"] forState:UIControlStateNormal];
    [takePhotoButton setAdjustsImageWhenHighlighted:NO];
    [takePhotoButton addTarget:self action:@selector(takePhotoPushed) forControlEvents:UIControlEventTouchUpInside];
    [mask addSubview:takePhotoButton];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(20, realScreenHeight-44-20, 30, 30)];
    [cancelButton setImage:[UIImage imageNamed:@"Cancel_Button_Camera.png"] forState:UIControlStateNormal];
    [cancelButton setAdjustsImageWhenHighlighted:NO];
    [cancelButton addTarget:self action:@selector(cancelPushed) forControlEvents:UIControlEventTouchUpInside];
    [mask addSubview:cancelButton];
    
    UIButton *switchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [switchButton setFrame:CGRectMake(SCREEN_WIDTH-20-30, 20, 30, 30)];
    [switchButton setImage:[UIImage imageNamed:@"Turn_Camera.png"] forState:UIControlStateNormal];
    [switchButton setAdjustsImageWhenHighlighted:NO];
    [switchButton addTarget:self action:@selector(switchPushed) forControlEvents:UIControlEventTouchUpInside];
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        [mask addSubview:switchButton];
    }
    
    UIButton *flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [flashButton setFrame:CGRectMake(20, 20, 30, 30)];
    [flashButton setImage:[UIImage imageNamed:@"Flash_Camera.png"] forState:UIControlStateNormal];
    [flashButton setTag:0];
    [flashButton setAdjustsImageWhenHighlighted:NO];
    [flashButton addTarget:self action:@selector(flashPushed:) forControlEvents:UIControlEventTouchUpInside];
    if ([UIImagePickerController isFlashAvailableForCameraDevice:UIImagePickerControllerCameraDeviceRear]) {
        [mask addSubview:flashButton];
    }
    [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
    
    //check for iOS 7
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending)
        imagePicker.cameraViewTransform = CGAffineTransformScale(imagePicker.cameraViewTransform, (float)realScreenHeight/(realScreenHeight-pixelsToFill*2), (float)realScreenHeight/(realScreenHeight-pixelsToFill*2));
    [imagePicker setCameraOverlayView:mask];*/
    [self presentViewController:imagePicker animated:YES completion:nil];
}
/*
-(void)takePhotoPushed {
    [imagePicker takePicture];
}

-(void)cancelPushed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)switchPushed {
    if ([imagePicker cameraDevice] == UIImagePickerControllerCameraDeviceRear) {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }
    else {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}

-(void)flashPushed:(UIButton *)sender {
    if (sender.tag == 0) {
        [sender setImage:[UIImage imageNamed:@"Flash_Camera_Off.png"] forState:UIControlStateNormal];
        [sender setTag:1];
        [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
    }
    else {
        [sender setImage:[UIImage imageNamed:@"Flash_Camera.png"] forState:UIControlStateNormal];
        [sender setTag:0];
        [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
    }
}
*/
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *returnImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    PHOTO_SOURCE source = PHOTO_SOURCE_LIBRARY;
    if ([picker sourceType] != UIImagePickerControllerSourceTypePhotoLibrary) {
        switch ([returnImage imageOrientation]) {
            case 0:
                source = PHOTO_SOURCE_CAMERA_LANDSCAPE_RIGHT;
                break;
            case 1:
                source = PHOTO_SOURCE_CAMERA_LANDSCAPE_LEFT;
                break;
            case 2:
                source = PHOTO_SOURCE_CAMERA_PORTRAIT_UPSIDEDOWN;
                break;
            case 3:
                source = PHOTO_SOURCE_CAMERA_PORTRAIT;
                break;
            default:
                break;
        }
    }
    [Settings setPhoto:returnImage withSource:source];
    [avatar update];

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)pageControlTouched:(UIPageControl *)pageControl {
    if (pageControl.currentPage < 4) {
        [self previousStep];
    }
    [pageControl setCurrentPage:4];
}

-(void)previousStep {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)finishUp {
    [[NSNotificationCenter defaultCenter] postNotificationName:SETTINGS_CHANGED_NOTIFICATION object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
