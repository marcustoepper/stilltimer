//
//  SettingsViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import "SettingsViewController.h"

@implementation SettingsViewController

@synthesize delegate;

-(void)loadView {
    self.view = [[SettingsView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [(SettingsView *)self.view setDelegate:self];
}


-(void)choosePhotoFromLibrary {
    imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)takePhoto {
    imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setDelegate:self];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    //[imagePicker setShowsCameraControls:NO];
    //[imagePicker setHidesBottomBarWhenPushed:YES];
    /*int realScreenHeight = 0;
    //int pixelsToFill = 54;
    UIImageView *mask;
    if (SCREEN_HEIGHT == 460) {
        realScreenHeight = 480;
        mask = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 480)] autorelease];
        [mask setImage:[UIImage imageNamed:@"Photomask_small.png"]];
    }
    else {
        realScreenHeight = 568;
        mask = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 568)] autorelease];
        [mask setImage:[UIImage imageNamed:@"Photomask_big.png"]];
    }
    [mask setUserInteractionEnabled:YES];
    
    UIButton *takePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [takePhotoButton setFrame:CGRectMake((SCREEN_WIDTH-58)/2, realScreenHeight-58-20, 58, 58)];
    [takePhotoButton setImage:[UIImage imageNamed:@"Snap_Button.png"] forState:UIControlStateNormal];
    [takePhotoButton setAdjustsImageWhenHighlighted:NO];
    [takePhotoButton addTarget:self action:@selector(takePhotoPushed) forControlEvents:UIControlEventTouchUpInside];
    [mask addSubview:takePhotoButton];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(20, realScreenHeight-44-20, 30, 30)];
    [cancelButton setImage:[UIImage imageNamed:@"Cancel_Button_Camera.png"] forState:UIControlStateNormal];
    [cancelButton setAdjustsImageWhenHighlighted:NO];
    [cancelButton addTarget:self action:@selector(cancelPushed) forControlEvents:UIControlEventTouchUpInside];
    [mask addSubview:cancelButton];
    
    UIButton *switchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [switchButton setFrame:CGRectMake(SCREEN_WIDTH-20-30, 20, 30, 30)];
    [switchButton setImage:[UIImage imageNamed:@"Turn_Camera.png"] forState:UIControlStateNormal];
    [switchButton setAdjustsImageWhenHighlighted:NO];
    [switchButton addTarget:self action:@selector(switchPushed) forControlEvents:UIControlEventTouchUpInside];
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        [mask addSubview:switchButton];
    }
    
    UIButton *flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [flashButton setFrame:CGRectMake(20, 20, 30, 30)];
    [flashButton setImage:[UIImage imageNamed:@"Flash_Camera.png"] forState:UIControlStateNormal];
    [flashButton setTag:0];
    [flashButton setAdjustsImageWhenHighlighted:NO];
    [flashButton addTarget:self action:@selector(flashPushed:) forControlEvents:UIControlEventTouchUpInside];
    if ([UIImagePickerController isFlashAvailableForCameraDevice:UIImagePickerControllerCameraDeviceRear]) {
        [mask addSubview:flashButton];
    }
    [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
    
    //check for iOS 7
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending)
        imagePicker.cameraViewTransform = CGAffineTransformScale(imagePicker.cameraViewTransform, (float)realScreenHeight/(realScreenHeight-pixelsToFill*2), (float)realScreenHeight/(realScreenHeight-pixelsToFill*2));*/
    //[imagePicker setCameraOverlayView:mask];
    [self presentViewController:imagePicker animated:YES completion:nil];
}
/*
-(void)takePhotoPushed {
    [imagePicker takePicture];
}

-(void)cancelPushed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)switchPushed {
    if ([imagePicker cameraDevice] == UIImagePickerControllerCameraDeviceRear) {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }
    else {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}

-(void)flashPushed:(UIButton *)sender {
    if (sender.tag == 0) {
        [sender setImage:[UIImage imageNamed:@"Flash_Camera_Off.png"] forState:UIControlStateNormal];
        [sender setTag:1];
        [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
    }
    else {
        [sender setImage:[UIImage imageNamed:@"Flash_Camera.png"] forState:UIControlStateNormal];
        [sender setTag:0];
        [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
    }
}
*/
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *returnImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    PHOTO_SOURCE source = PHOTO_SOURCE_LIBRARY;
    if ([picker sourceType] != UIImagePickerControllerSourceTypePhotoLibrary) {
        switch ([returnImage imageOrientation]) {
            case 0:
                source = PHOTO_SOURCE_CAMERA_LANDSCAPE_RIGHT;
                break;
            case 1:
                source = PHOTO_SOURCE_CAMERA_LANDSCAPE_LEFT;
                break;
            case 2:
                source = PHOTO_SOURCE_CAMERA_PORTRAIT_UPSIDEDOWN;
                break;
            case 3:
                source = PHOTO_SOURCE_CAMERA_PORTRAIT;
                break;
            default:
                break;
        }
    }
    [Settings setPhoto:returnImage withSource:source];
    [(SettingsView *)self.view reloadViews];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SETTINGS_CHANGED_NOTIFICATION object:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)cancel {
    [self end];
}

-(void)confirm {
    [(SettingsView *)self.view everythingOut];
    if (![[[(SettingsView *)self.view nameField] text] isEqualToString:NSLocalizedString(@"DefaultNameKey", @"")] && ![[[(SettingsView *)self.view nameField] text] isEqualToString:@""] && [[(SettingsView *)self.view nameField] text] != nil) {
        [Settings setName:[[(SettingsView *)self.view nameField] text]];
        [Settings setBirthday:[[(SettingsView *)self.view birthdayPicker] getDate]];
        if ([[(SettingsView *)self.view genderPicker] getActiveRowForComponent:0] == 0) {
            [Settings setGender:GENDER_MALE];
        }
        else {
            [Settings setGender:GENDER_FEMALE];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SETTINGS_CHANGED_NOTIFICATION object:nil];
        [self end];
    }
    else {
        alert = [[CustomAlertView alloc] initWithMessage:NSLocalizedString(@"EmptyNameImpossibleAlertMessageKey", @"") buttonTitles:@[NSLocalizedString(@"OKKey", @"")]];
        alert.delegate = self;
        alert.tag = 1;
        [alert show];
    }
}

-(void)deleteEverything {
    alert = [[CustomAlertView alloc] initWithMessage:NSLocalizedString(@"DeleteAllAlertMessageKey", @"") buttonTitles:@[NSLocalizedString(@"DeleteAllAlertKeepKey", @""), NSLocalizedString(@"DeleteAllAlertDeleteKey", @"")]];
    alert.delegate = self;
    alert.tag = 2;
    [alert show];
}

-(void)buttonPushed:(int)buttonId alertViewTag:(int)tag {
    if (tag == 2) {
        if (buttonId == 1) {
            [self deleteEverythingConfirmed];
        }
    }
    else if (tag == 1) {
        [[(SettingsView *)self.view nameField] becomeFirstResponder];
    }
}

-(void)deleteEverythingConfirmed {
    [Settings wipe];
    [[StillData getInstance] wipe];
    
    [delegate startIntro];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SETTINGS_CHANGED_NOTIFICATION object:nil];
    [self end];
}

-(void)end {
    [delegate dismissSettingsView];
    [(SettingsView *)self.view reloadViews];
}

@end
