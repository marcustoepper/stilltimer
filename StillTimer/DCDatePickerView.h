//
//  DVDatePickerView.h
//  StillTimer
//
//  Created by Alexander Schilling on 6/18/13.
//
//

#import "DCPickerView.h"


//first year to display
#define DV_DATE_PICKER_STARTING_YEAR 2010
//last year to display
#define DV_DATE_PICKER_ENDING_YEAR 2030

@interface DCDatePickerView : DCPickerView

//init method
//point: top left point of the picker
-(id)initAtPoint:(CGPoint)point;


//! if current year is not in year range, this method won't work
//scrolls to the current day in the local timezone
-(void)scrollToTodayAnimated:(BOOL)animated;

//! date needs to be in year range, otherwise this method won't work
//scroll to specified date in the local timezone
-(void)scrollToDate:(NSDate *)date animated:(BOOL)animated;

//get the currently selected date at 12AM
-(NSDate *)getDate;

@end
