//
//  StringBuilder.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "StringBuilder.h"

@implementation StringBuilder

+(NSString *)getAgeString {
    NSDate *birthday = [Settings getBirthday];
    double age = [[NSDate date] timeIntervalSince1970]-[birthday timeIntervalSince1970];
    int daysAge = age/(60*60*24);
    int weeksAge = daysAge/7;
    if (daysAge < 14) {
        return [NSString stringWithFormat:NSLocalizedString(@"DaysAgeKey", @""), daysAge];
    }
    else {
        return [NSString stringWithFormat:NSLocalizedString(@"WeeksAgeKey", @""), weeksAge];
    }
}

+(NSString *)getLastFeedingStartedString {
    if (![[StillData getInstance] emptyData]) {
        double time = [[[[[StillData getInstance] getStillTimes] objectAtIndex:0] objectForKey:@"startTime"] intValue];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([language isEqualToString:@"de"]) {
            [formatter setDateFormat:@"HH:mm"];
        }
        else {
            [formatter setDateFormat:@"h:mm a"];
        }
        return [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time]];
    }
    return NSLocalizedString(@"NoFeedingKey", @"");
}

+(NSString *)getLastFeedingString {
    if ([[StillData getInstance] getStillTimesCount] != 0) {
        double timespan = [[NSDate date] timeIntervalSince1970]-[[[[[StillData getInstance] getStillTimes] objectAtIndex:0] objectForKey:@"endTime"] intValue];
        int minutes = round(timespan/60.0);
        int hours = round(minutes/60.0);
        if (minutes == 0) {
            return [NSString stringWithFormat:NSLocalizedString(@"MinutesAgoKey", @""), 1];
        }
        else if (minutes < 60) {
            return [NSString stringWithFormat:NSLocalizedString(@"MinutesAgoKey", @""), minutes];
        }
        else {
            minutes = minutes % 60;
            if (minutes < 10) {
                return [NSString stringWithFormat:NSLocalizedString(@"HoursAgoMinutesDigitKey", @""), hours, minutes];
            }
            else {
                return [NSString stringWithFormat:NSLocalizedString(@"HoursAgoKey", @""), hours, minutes];
            }
        }
    }
    return NSLocalizedString(@"NoFeedingKey", @"");
}

+(NSString *)getLastFeedingDurationString {
    if ([[StillData getInstance] getStillTimesCount] != 0) {
        double timespan = [[[[[StillData getInstance] getStillTimes] objectAtIndex:0] objectForKey:@"endTime"] doubleValue]-[[[[[StillData getInstance] getStillTimes] objectAtIndex:0] objectForKey:@"startTime"] doubleValue];
        int duration = (int)timespan;
        duration = (int)round(duration/60.0);
        if (duration == 0) duration = 1;
        return [NSString stringWithFormat:@"%d %@",duration,NSLocalizedString(@"MinKey", @"")];
    }
    return NSLocalizedString(@"NoFeedingKey", @"");
}

+(NSString *)getFeedingsTodayString {
    if ([[[StillData getInstance] getSectionedStillTimes] count] != 0) {
        int feedings = [[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:0] count];
        int feedday = [[[[[[StillData getInstance] getSectionedStillTimes] objectAtIndex:0] objectAtIndex:0] objectForKey:@"endTime"] intValue];
        int today = [[NSDate date] timeIntervalSince1970];
        today = today - (today%(60*60*24));
        if (feedday - today < 0) {
            return NSLocalizedString(@"NoFeedingTodayKey", @"");
        }
        if (feedings == 1) {
            return NSLocalizedString(@"OneFeedingTodayKey", @"");
        }
        else {
            return [NSString stringWithFormat:NSLocalizedString(@"FeedingsTodayKey", @""),feedings];
        }
    }
    return NSLocalizedString(@"NoFeedingTodayKey", @"");
}

+(NSString *)getStringForSide:(int)side {
    switch (side) {
        case SIDE_LEFT:
            return NSLocalizedString(@"LeftKey", @"");
        case SIDE_RIGHT:
            return NSLocalizedString(@"RightKey", @"");
        case SIDE_BOTH_LEFT_FIRST:
            return NSLocalizedString(@"BothLeftFirstKey", @"");
        case SIDE_BOTH_RIGHT_FIRST:
            return NSLocalizedString(@"BothRightFirstKey", @"");
        default:
            return NSLocalizedString(@"LeftKey", @"");
    }
}

+(NSString *)getStringForTime:(double)interval {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"de"]) {
        [formatter setDateFormat:@"HH:mm"];
    }
    else {
        [formatter setDateFormat:@"h:mm a"];
    }
    return [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:interval]];
}

+(NSString *)getStringForDuration:(int)duration {
    duration = (int)round(duration/60.0);
    if (duration == 0) duration = 1;
    return [NSString stringWithFormat:@"%d %@",duration,NSLocalizedString(@"MinKey", @"")];
}

+(NSString *)getStringWithSecondsForDuration:(int)duration {
    if (duration == 0) duration = 1;
    if (duration%60 < 10) {
        return [NSString stringWithFormat:@"%d:0%d %@",duration/60,duration%60,NSLocalizedString(@"MinKey", @"")];
    }
    else {
        return [NSString stringWithFormat:@"%d:%d %@",duration/60,duration%60,NSLocalizedString(@"MinKey", @"")];
    }
}

@end
