//
//  SettingsView.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import <UIKit/UIKit.h>
#import "DCDatePickerView.h"
#import "AvatarView.h"
#import "ImprintViewController.h"

@protocol SettingsViewDelegate <NSObject>

-(void)choosePhotoFromLibrary;
-(void)takePhoto;

-(void)cancel;
-(void)confirm;

-(void)deleteEverything;

@end

@interface SettingsView : UIView <UIActionSheetDelegate, UITextFieldDelegate, DVPickerViewDelegate>

@property (strong, nonatomic) id <SettingsViewDelegate> delegate;

@property (strong, nonatomic) AvatarView *avatar;

@property (strong, nonatomic) UIScrollView *settingsScroll;

@property (strong, nonatomic) UIButton *backButton;

@property (strong, nonatomic) UITextField *nameField;
@property (strong, nonatomic) UIButton *birthdayButton;
@property (strong, nonatomic) UILabel *birthdayLabel;
@property (strong, nonatomic) DCDatePickerView *birthdayPicker;
@property (strong, nonatomic) UIButton *genderButton;
@property (strong, nonatomic) UILabel *genderLabel;
@property (strong, nonatomic) DCPickerView *genderPicker;

-(void)reloadViews;
-(void)everythingOut;

@end
