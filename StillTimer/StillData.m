//
//  RoomsModel.m
//  myTelco
//
//  Created by Stefan Bayer on 22.06.12.
//  Copyright (c) 2012 deVision coding GmbH. All rights reserved.
//

#import "StillData.h"

#pragma mark Member Setup
static StillData *stillData = nil;

@implementation StillData

@synthesize databaseName, databasePath;

#pragma mark -
#pragma mark Singleton Methods
+ (StillData *)getInstance {
	@synchronized(self) {
		if(!stillData) {
			stillData = [[self alloc] init];
		}
		return stillData;
	}
	return nil;
}

+ (id)alloc {
	stillData = [super alloc];
	return stillData;
}

#pragma mark -
#pragma mark Setup
- (StillData *) init {
	self = [super init];
	
	// Setup some globals
	self.databaseName = @"stilldatabase.sqlite";
	
	// Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	
	self.databasePath = [[NSString alloc] init];
	self.databasePath = [documentsDir stringByAppendingPathComponent:self.databaseName];
	[self checkAndCreateDatabase];
	
	return self;
}

-(void) checkAndCreateDatabase {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseName];
	[fileManager copyItemAtPath:databasePathFromApp toPath:self.databasePath error:nil];
}

- (NSMutableArray *)getStillTimes {
    if (_times == nil) {
        NSMutableArray *resultSet = [[NSMutableArray alloc] init];
        
        sqlite3 *database;
        if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
            
            NSMutableString * select = [NSMutableString stringWithString: @"SELECT id,startTime,endTime,side FROM stillTimes ORDER BY startTime DESC;"];
            
            const char *sqlStatement = [select UTF8String];
            
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
                    
                    NSNumber *timeID = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 0)];
                    [result setObject:timeID forKey:@"id"];
                    
                    NSString *startTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                    [result setObject:startTime forKey:@"startTime"];
                    
                    NSString *endTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                    [result setObject:endTime forKey:@"endTime"];
                    
                    NSNumber *side = [NSNumber numberWithInt:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)] intValue]];
                    [result setObject:side forKey:@"side"];
                    
                    [resultSet addObject:result];
                }
            }
            sqlite3_finalize(compiledStatement);
        }
        sqlite3_close(database);
        
        _times = [resultSet mutableCopy];
        return resultSet;
    }
    else {
        return _times;
    }
}

-(NSMutableArray *)getSectionedStillTimes {
    if (_sectionedTimes == nil) {
        NSMutableArray *timesArray = [[self getStillTimes] mutableCopy];
        NSMutableArray *sectionedArray = [[NSMutableArray alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.YYYY"];
        
        NSMutableArray *dayArray = [[NSMutableArray alloc] init];
        NSString *currentDay = @"";
        for (NSDictionary *dict in timesArray) {
            NSString *dictDay = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[dict objectForKey:@"startTime"] doubleValue]]];
            if (![currentDay isEqualToString:dictDay]) {
                if ([dayArray count] != 0) {
                    [sectionedArray addObject:[dayArray copy]];
                    [dayArray removeAllObjects];
                }
                currentDay = [dictDay copy];
            }
            [dayArray addObject:[dict copy]];
        }
        if ([dayArray count] != 0) {
            [sectionedArray addObject:[dayArray copy]];
            [dayArray removeAllObjects];
        }
        
        _sectionedTimes = [sectionedArray mutableCopy];
        return sectionedArray;
    }
    else {
        return _sectionedTimes;
    }
}

- (void)insertStillTimeWithStart:(double)start end:(double)end andSide:(int)side
{
    sqlite3 *database;
	
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
		
		const char *sqlStatement = "INSERT INTO stillTimes (startTime,endTime,side) VALUES (?,?,?);";
		
		sqlite3_stmt *compiled_statement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiled_statement, NULL) == SQLITE_OK) {
            
            sqlite3_bind_double(compiled_statement, 1, start);
            sqlite3_bind_double(compiled_statement, 2, end);
            sqlite3_bind_int(compiled_statement, 3, side);
            
		}
		
        if(sqlite3_step(compiled_statement) != SQLITE_DONE ) {
		} else {
		}
		sqlite3_finalize(compiled_statement);
	}
	
	sqlite3_close(database);
    
    _times = nil;
    _sectionedTimes = nil;
}

- (void)deleteStillTimeWithId:(int)stillTimeId
{
    int realId = [[[[self getStillTimes] objectAtIndex:stillTimeId] objectForKey:@"id"] intValue];
    
    sqlite3 *database;
    if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "DELETE FROM stillTimes WHERE id = ?;";
        
        sqlite3_stmt *compiled_statement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiled_statement, NULL) == SQLITE_OK) {
            
            sqlite3_bind_int(compiled_statement, 1, realId);
            
        }
        if(sqlite3_step(compiled_statement) != SQLITE_DONE ) {
        } else {
        }
        sqlite3_finalize(compiled_statement);
    }
    
    sqlite3_close(database);
    
    _times = nil;
    _sectionedTimes = nil;
}

- (void)updateStillTime:(int)stillTimeId toStart:(double)start end:(double)end andSide:(int)side
{
    int realId = [[[[self getStillTimes] objectAtIndex:stillTimeId] objectForKey:@"id"] intValue];

    sqlite3 *database;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
		
		const char *sqlStatement = "UPDATE stillTimes SET startTime = ?, endTime = ?, side = ? WHERE id = ?;";
		
		sqlite3_stmt *compiled_statement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiled_statement, NULL) == SQLITE_OK) {
			
			sqlite3_bind_double(compiled_statement, 1, start);
			sqlite3_bind_double(compiled_statement, 2, end);
			sqlite3_bind_int(compiled_statement, 3, side);
			sqlite3_bind_int(compiled_statement, 4, realId);
			
		}
		if(sqlite3_step(compiled_statement) != SQLITE_DONE ) {
		} else {
		}
		sqlite3_finalize(compiled_statement);
	}
	
	sqlite3_close(database);
    
    _times = nil;
    _sectionedTimes = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:DATA_UPDATE_NOTIFICATION object:nil];
}

- (int)getStillTimesCount
{
    int count = 0;
	
	sqlite3 *database;
	if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
		
		NSMutableString * select = [NSMutableString stringWithString: @"SELECT COUNT(id) FROM stillTimes;"];
		
		const char *sqlStatement = [select UTF8String];
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				
                count = sqlite3_column_int(compiledStatement, 0);
                
			}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
	return count;
}

-(int)getIdWithIndexPath:(NSIndexPath *)indexPath {
    int stillTimeId = 0;
    NSArray *sectionedArray = [self getSectionedStillTimes];
    
    for (int i=0;i<indexPath.section;i++) {
        stillTimeId += [[sectionedArray objectAtIndex:i] count];
    }
    
    stillTimeId += indexPath.row;

    return stillTimeId;
}

-(BOOL)emptyData {
    return ([[self getStillTimes] count] == 0);
}

-(void)wipe {
    sqlite3 *database;
    if(sqlite3_open([self.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "DELETE FROM stillTimes;";
        
        sqlite3_stmt *compiled_statement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiled_statement, NULL) == SQLITE_OK) {
            
        }
        if(sqlite3_step(compiled_statement) != SQLITE_DONE ) {
        } else {
        }
        sqlite3_finalize(compiled_statement);
    }
    
    sqlite3_close(database);
    
    _times = nil;
    _sectionedTimes = nil;
}

@end
