//
//  MainViewController.h
//  StillTimer
//
//  Created by Alexander Schilling on 5/26/13.
//
//

#import <UIKit/UIKit.h>
#import "MainView.h"
#import "IntroViewController.h"
#import "SettingsViewController.h"
#import "StillViewController.h"
#import "AddEditViewController.h"

@interface MainViewController : UIViewController <StillViewControllerDelegate, AddEditViewControllerDelegate, SettingsViewControllerDelegate>

@property (strong, nonatomic) IntroViewController *introController;
@property (strong, nonatomic) SettingsViewController *settingsController;
@property (strong, nonatomic) StillViewController *stillController;
@property (strong, nonatomic) AddEditViewController *addEditController;

@end
