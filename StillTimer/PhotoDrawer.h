//
//  PhotoDrawer.h
//  StillTimer
//
//  Created by Alexander Schilling on 8/26/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PhotoDrawer : UIView

@property CGPoint p;
@property float scale;
@property (strong, nonatomic) UIImage *image;

-(void)update;

-(void)applyOffset:(CGPoint)offset;

@end
