//
//  CustomAlertView.h
//  StillTimer
//
//  Created by Alexander Schilling on 9/26/13.
//
//

#import <UIKit/UIKit.h>

@protocol CustomAlertViewDelegate <NSObject>

-(void)buttonPushed:(int)buttonId alertViewTag:(int)tag;

@end

@interface CustomAlertView : UIViewController

@property (nonatomic, strong) id <CustomAlertViewDelegate> delegate;

@property int tag;

- (id)initWithMessage:(NSString *)msg buttonTitles:(NSArray *)buttonTitles;

-(void)show;
-(void)dismiss;

@end
