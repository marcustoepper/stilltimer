//
//  FourthStepViewController.m
//  StillTimer
//
//  Created by Alexander Schilling on 5/14/13.
//
//

#import "FourthStepViewController.h"

@interface FourthStepViewController ()

@end

@implementation FourthStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIButton *prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [prevButton setFrame:CGRectMake(10, 10, 50, 50)];
        [prevButton addTarget:self action:@selector(previousStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:prevButton];
        
        UIImageView *prevImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [prevImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_LEFT_ARROW_BUTTON]];
        [prevButton addSubview:prevImage];
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextButton setFrame:CGRectMake(SCREEN_WIDTH-60, 10, 50, 50)];
        [nextButton addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:nextButton];
        
        UIImageView *nextImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        [nextImage setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_RIGHT_ARROW_BUTTON]];
        [nextButton addSubview:nextImage];
        
        UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 70, 240, 60)];
        [questionLabel setText:NSLocalizedString(@"BirthdayQuestionKey", @"")];
        [questionLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:19]];
        [questionLabel setTextColor:[UIColor colorWithWhite:159.0/255.0 alpha:1.0]];
        [questionLabel setBackgroundColor:[UIColor clearColor]];
        [questionLabel setTextAlignment:NSTextAlignmentCenter];
        [questionLabel setNumberOfLines:0];
        [self.view addSubview:questionLabel];
        
        UIButton *dateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [dateButton setFrame:CGRectMake(49, 140, 221, 68)];
        [dateButton setImage:[ImageProvider getGenderedImageForPurpose:IMAGE_PURPOSE_FIELD_BIG] forState:UIControlStateNormal];
        [dateButton addTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:dateButton];
        
        dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 221, 68)];
        [dateLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [dateLabel setTextAlignment:NSTextAlignmentCenter];
        [dateLabel setTextColor:[UIColor colorWithWhite:175.0/255.0 alpha:1.0]];
        [dateLabel setUserInteractionEnabled:NO];
        [dateLabel setBackgroundColor:[UIColor clearColor]];
        [dateButton addSubview:dateLabel];
        
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-60, SCREEN_WIDTH, 60)];
        [pageControl setNumberOfPages:5];
        [pageControl setCurrentPage:3];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            [pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:205.0/255.0 alpha:1.0]];
            [pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:158.0/255.0 alpha:1.0]];
        }
        [pageControl addTarget:self action:@selector(pageControlTouched:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:pageControl];
        
        datePicker = [[DCDatePickerView alloc] initAtPoint:CGPointMake(0, SCREEN_HEIGHT)];
        [datePicker setDelegate:self];
        [datePicker scrollToTodayAnimated:NO];
        [self.view addSubview:datePicker];
        
        [self updateDateLabel];
        
        UISwipeGestureRecognizer *prevRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(previousStep)];
        [prevRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.view addGestureRecognizer:prevRecognizer];
        
        UISwipeGestureRecognizer *nextRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextStep)];
        [nextRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.view addGestureRecognizer:nextRecognizer];
    }
    return self;
}

-(void)pickerView:(DCPickerView *)pickerView didSelectRow:(int)row inComponent:(int)component {
    [self updateDateLabel];
}

-(void)animateDatePickerIn:(UIButton *)sender {
    [sender removeTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
    [sender addTarget:self action:@selector(animateDatePickerOut:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [datePicker setFrame:CGRectMake(0, SCREEN_HEIGHT-datePicker.frame.size.height-35, SCREEN_WIDTH, datePicker.frame.size.height)];
    
    [UIView commitAnimations];
}

-(void)updateDateLabel {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *date = [datePicker getDate];
    
    if ([[formatter stringFromDate:date] isEqualToString:[formatter stringFromDate:[NSDate date]]]) {
        [dateLabel setText:NSLocalizedString(@"TodayKey", @"")];
    }
    else if ([[formatter stringFromDate:date] isEqualToString:[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970] - (24*60*60)]]]) {
        [dateLabel setText:NSLocalizedString(@"YesterdayKey", @"")];
    }
    else {
        [dateLabel setText:[[formatter stringFromDate:date] lowercaseString]];
    }
}

-(void)animateDatePickerOut:(UIButton *)sender {
    [sender removeTarget:self action:@selector(animateDatePickerOut:) forControlEvents:UIControlEventTouchUpInside];
    [sender addTarget:self action:@selector(animateDatePickerIn:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView beginAnimations:nil context:nil];
    
    [datePicker setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, datePicker.frame.size.height)];
    
    [UIView commitAnimations];
}

-(void)pageControlTouched:(UIPageControl *)pageControl {
    if (pageControl.currentPage > 3) {
        [self nextStep];
    }
    else if (pageControl.currentPage < 3) {
        [self previousStep];
    }
    [pageControl setCurrentPage:3];
}

-(void)previousStep {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)nextStep {
    int picked = [[datePicker getDate] timeIntervalSince1970];
    picked = picked - picked%(60*60*24);
    [Settings setBirthday:[NSDate dateWithTimeIntervalSince1970:picked]];
    
    controller = [[FifthStepViewController alloc] init];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
